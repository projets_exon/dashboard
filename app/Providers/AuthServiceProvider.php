<?php
namespace App\Providers;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\User;
use View;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
public function boot()
{
  view()->composer('*', function($view){
  if(!empty(Auth::user()->id))
  {
		$IdUser = Auth::user()->id;
		 $User = User::where('id', $IdUser)
        ->get();
	 
	    foreach($User as $post)
	    {
	    $UserId = $post->id; 
	    }
		View::share('UserId', $UserId);

  }
  elseif(empty(Auth::user()->id))
  {
        $IdUser = Null;
  }
  });	 
	
}
}
