<?php
namespace App\Providers;
use Illuminate\Support\Facades\Schema;
use DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Notifications;
use App\Image_favicon;
use App\Language;
use App\User;
use App\Category;
use App\Product;
use View;
use Session;
use Laravel\Cashier\Cashier;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    { //
        Cashier::ignoreMigrations();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

  // \URL::forceScheme('https');
	 $Language = Language::where('active',1)
     ->get();
     View::share('Language', $Language);
	 

     $language = Language::where('active',1)
     ->get();
     View::share('language', $language);


}
}
