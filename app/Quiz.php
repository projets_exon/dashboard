<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Quiz extends Model
{
protected $table = 'quiz';
protected $fillable = ['question','proposition_1','proposition_2','proposition_3','proposition_4','reponse','point'];

public function point_quiz($idPoint)
{
    $title = DB::select('select title from point where idPoint='.$idPoint.' limit 1');
    return $title[0]->title;
}
}
