<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Category;

class PhotoController extends Controller 
{
   public function index() 
   {
	   if(Auth::check()){
        $users = DB::select('select * from users where id="'.ucfirst(Auth()->user()->id).'"');
      return view('user/user_view',['users'=>$users]);
      }
      
   }
   public function show() 
   {
	   
	    $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
      $users = DB::select('select * from users where id="'.ucfirst(Auth()->user()->id).'"');
      return view('user/user_update_pic', $data,['users'=>$users]);
   }
   public function edit(Request $request) 
   {
	   
	   
	      $this->validate($request,[
          'image' =>'required',
          'image.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
      if ($request->hasFile('image')) {
          $image = $request->file('image');
          foreach ($image as $files) {
              $destinationPath = 'public/profile_pic/user/';
              $file_name = time() . "." . $files->getClientOriginalExtension();
              $files->move($destinationPath, $file_name);
              $data[] = $file_name;
          }
      }
	  
      DB::update('update users set image = ? where id = ?',[$file_name,ucfirst(Auth()->user()->id)]);
      return redirect('user/setting')->with('success', 'Пост успешно добавлен!');

   }
   
 

}