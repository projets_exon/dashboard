<?php
namespace App\Http\Controllers\user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator,Redirect,Response;
use DB;
use App\Product;
use Cart;
use App\Category;
use App\Orders;
use App\Items;
use Session;

class CartdetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index($id)
    {
        
	    $datas['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
		
		$Orders = DB::table('orders')
		->where('id', '=', "".$id."")
        ->get();
        
	    $Items = DB::table('items')
		->where('orders_id', '=', "".$id."")
        ->count('id');
		
		
	   $Product = DB::table('products')
       ->join('items', 'products.id', '=', 'items.brand')
       ->join('orders', 'items.orders_id', '=', 'orders.id')
       ->select('products.product_name', 'products.photo', 'products.product_price', 'orders.currency', 'items.quantity', 'items.id')
       ->where('orders_id', '=', "".$id."")
       ->get();
	   
        return view('user.cart_details.index',$datas,compact('Items','Product','Orders'));

    }

   public function edit(Request $request) 
   {
      $id_order = $request->input('id_order');
      $firstname = $request->input('firstname');
      $lastname = $request->input('lastname');
      $tel = $request->input('tel');
      $mobile = $request->input('mobile');
      $country = $request->input('country');
      $city = $request->input('city');
      $address = $request->input('address');
      $postcode = $request->input('postcode');
      $updated_at = $request->input('updated_at');
      DB::update('update users set firstname = ?, lastname = ?, tel = ?, mobile = ?, country = ?, city = ?, address = ?, postcode = ?, updated_at = ? where id = ?',[$firstname,$lastname,$tel,$mobile,$country,$city,$address,$postcode,$updated_at,ucfirst(Auth()->user()->id)]);
      return redirect('cart/delivery/'.$request->input('id_order').'')->with('success', 'Nous vous remercions de votre fidélité');
   }  
}
