<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class TaskController extends Controller 
{
   public function task() 
   {
	  
      if(Auth::check()){
	  $users = DB::table('task')
	  ->where('id_user', '=', "".ucfirst(Auth()->user()->id)."")
	  ->count('id_user');
      return view('user/user_task',['users'=>$users]);
      }
      
   }


}