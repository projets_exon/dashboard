<?php
namespace App\Http\Controllers\user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator,Redirect,Response;
use DB;
use App\Product;
use Cart;
use App\Category;
use App\Orders;
use App\Items;
use Session;


class CartdeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
         if(Auth::check()){
	    $datas['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
		
		$Orders = DB::table('orders')
		->where('id', '=', "".$id."")
        ->get();
        
	    $Items = DB::table('items')
		->where('orders_id', '=', "".$id."")
        ->count('id');
		
		
	   $Product = DB::table('products')
       ->join('items', 'products.id', '=', 'items.product_name')
       ->join('orders', 'items.orders_id', '=', 'orders.id')
       ->select('products.product_name', 'products.photo', 'products.product_price', 'orders.currency', 'items.quantity', 'items.id')
       ->get();
	   
        return view('user.cart_details.delivery',$datas,compact('Items','Product','Orders'));
		 }
      return redirect::route('index.store');

    }





 public function edit(Request $request) 
   {
      $id_order = $request->input('id_order');
      $delivery = $request->input('delivery');
      $updated_at = $request->input('updated_at');
      DB::update('update orders set delivery = ?, updated_at = ? where id = ?',[$delivery,$updated_at,$id_order]);
      return redirect('cart/payment/'.$request->input('id_order').'')->with('success', 'Update with success!');

   }
    
}
