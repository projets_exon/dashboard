<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
Use App\User;
use App\Category;


class UserController extends Controller 
{
   public function index() 
   {
	   if(Auth::check())
	   {
       $users = DB::select('select * from users where id="'.ucfirst(Auth()->user()->id).'"');
      return view('user/user_view',['users'=>$users]);
      }
	  	   return redirect('login');

      
   }
   
   
    
    public function dashboard()
    {

      if(Auth::check()){
       $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
	   $myorders = DB::table('orders')
	   ->where('customer_id', '=', "".ucfirst(Auth()->user()->id)."")
       ->count('id');
	   
	   $myitems = DB::table('items')
	   ->where('brand', '=', "".ucfirst(Auth()->user()->id)."")
       ->count('id');
		
    return view('user.dashboard',$data, compact('myorders', 'myitems'));
	 
	
      }
    }

    public function profile()
    {

      if(Auth::check()){
		  $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
        return view('user.profile',$data);
      }
       return Redirect::to("login")->withSuccess('Opps! You do not have access');
    }
  


   public function address() 
   {
      if(Auth::check()){
       $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
		$users = DB::select('select * from users where id="'.ucfirst(Auth()->user()->id).'"');
      return view('user/address',$data, ['users'=>$users]);
   }
   }


   
   
   public function show() 
   {
      if(Auth::check()){
       $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
		$users = DB::select('select * from users where id="'.ucfirst(Auth()->user()->id).'"');
      return view('user/user_update',$data, ['users'=>$users]);
   }
   }
   
   
   
   public function edit(Request $request) 
   {
      $firstname = $request->input('firstname');
      $lastname = $request->input('lastname');
      $email = $request->input('email');
      $country = $request->input('country');
      $updated_at = $request->input('updated_at');
      DB::update('update users set firstname = ?, lastname = ?, email = ?, country = ?, updated_at = ? where id = ?',[$firstname,$lastname,$email,$country,$updated_at,ucfirst(Auth()->user()->id)]);
      return redirect('user/profile')->with('success', 'Update with success!');

   }
   
    public function setting()
    {

      if(Auth::check())
	  {
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		
        return view('user.setting',$data);
      }
	   return redirect('login');

    }


}