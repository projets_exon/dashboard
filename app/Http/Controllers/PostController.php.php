<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Cart;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
	public function index(){
        $posts=\App\Post::all();
        return view('posts',['posts'=>$posts]);
    }
    // Post Detail
    public function detail(Request $request,$id){
        $post=\App\Post::find($id);
        return view('post-detail',['post'=>$post]);
    }
}
