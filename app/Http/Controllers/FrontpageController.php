<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class FrontpageController extends Controller
{
    public function home()
    {
      $product=Product::all();
      return view('front_page.front_page',compact('product'));    
	}
}