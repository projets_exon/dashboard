<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
Use App\User;
use App\Product;
use App\Orders;
use App\Items;
use App\Post;
use App\Category;
use App\Cart;

use App\Purchase;

class ProductsController extends Controller
{
		public function Payer(Request $request)
    {
        $s=$request->input("payer");
        return redirect('/'.$s.'/'.$request->route('id'));

    }

	public function testcart()
    {


		$products = Product::all();

		$products_electro = Product::all()
	    ->where('category_id', '=', "1");

		$products_women = Product::all()
	    ->where('category_id', '=', "2");

		$products_men = Product::all()
	    ->where('category_id', '=', "3");
        return view('testcart', compact('products_women','products'));
    }

    public function testcartlist()
    {
        $products = Product::all();
        return view('testcartlist', compact('products'));
    }

    public function index()
    {
        $products = Product::all();
        return view('products', compact('products'));
    }


	public function listproduit()
    {

		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();

        $products = Product::all();


        return view('home',$data, compact('products'));
    }



	public function listproduitdollar($id)
    {

		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();

        $products = Product::all();


        return view('home',$data, compact('products'));
    }

    public function cart()
    {
	   if(Auth::check())
	   {
		 $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();

		$users = DB::table('users')
	    ->where('id_user', '=', "".ucfirst(Auth()->user()->id_user)."");
		return view('cart',$data, compact('users'));
       }
	   $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
		return view('cart',$data);

    }





	public function store(Request $request)
    {
        $data=$request->all();
        $lastid=Purchase::create($data)->id;
        if(count($request->list_product) > 0)
        {
        foreach($request->list_product as $item=>$v){
            $data2=array(
                'list_product'=>$request->list_product[$item],
                'id_user'=>$request->id_user[$item],
                'active'=>$request->active[$item],
                'total'=>$request->total[$item],
                'statut'=>$request->statut[$item],
            );
        Purchase::insert($data2);
      }
        }
        return redirect()->back()->with('success','data insert successfully');
    }


	public function show($id)
    {
		$data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();

        $products = Product::find($id);

        return view('show',$data, compact('products'));
    }






    public function products_seo($id)
    {

		if(Auth::check()){

		 $notifications = DB::table('notifications')
		->where('status', '=', "0")
        ->get();

		 $Product = DB::table('products')
		->where('products.product_name', '=', $id)
	    ->get();

		$Category_product = DB::table('products')
		->join('category_products', 'products.id', '=', 'category_products.id_product')
		->join('category', 'category_products.id_category', '=', 'category.id')
		->select('category.id', 'category.name', 'category.parent_id')
		->where('products.product_name', '=', $id)
	    ->get();

	      $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();



        $Category = Category::where('parent_id',0)->get();

        return view('web.products.show',$data, compact('Product', 'notifications', 'Category_product'));
		}

		return redirect::to("admin");

    }



    public function addToCart($id)
    {

        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
        } else {
            $cart = new Cart();
        }
        $product=Product::find($id);
        $cart->add($product);
        session()->put('cart', $cart);
return redirect()->back()->with('success', 'Product added to cart successfully!');
//return $cart->totalQty;
    }



    public function showCart()
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
        } else {
            $cart = null;
        }
        $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
       return view('cart',$data, compact('cart'));
    }


    public function showCart_de()
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
        } else {
            $cart = null;
        }
        $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
        return view('web.de.cart.index',$data, compact('cart'));
    }

    public function showCart_fr()
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
        } else {
            $cart = null;
        }
        $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
       return view('web.fr.cart.index',$data, compact('cart'));
    }


    public function showCart_en()
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
        } else {
            $cart = null;
        }
        $data['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
       return view('web.en.cart.index',$data, compact('cart'));
    }



	public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');
            $cart->items[$request->id]["qty"] = $request->quantity;
            $somme=0;
            $sommePrice = 0;
            foreach ($cart->items as $product){
                $somme +=$product["qty"];
                $sommePrice +=$product["price"] * $product["qty"];
            }
            $cart->totalQty = $somme;
            $cart->totalPrice = $sommePrice;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
            echo "success";
        }
    }

    public function remove(Request $request)
    {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart->items[$request->id])) {
                unset($cart->items[$request->id]);
                $somme=0;
                $sommePrice = 0;
                foreach ($cart->items as $product){
                    $somme +=$product["qty"];
                    $sommePrice +=$product["price"] * $product["qty"];
                }
                $cart->totalQty = $somme;
                $cart->totalPrice = $sommePrice;
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

	function save_comment(Request $request)
	{
        $data=new \App\Comment;
        $data->post_id=$request->post;
        $data->comment_text=$request->comment;
        $data->save();
        return response()->json([
            'bool'=>true
        ]);
    }

    function getProductsByCat(Request $request) {
        $catIds = $request->get('catIds');
        $lang = $request->get('lang');
        if($catIds != null) {
            $sqlArray='';
            foreach($catIds as $element){
                $sqlArray=$sqlArray. $element .',';
            }
            $sqlArray= substr($sqlArray, 0, -1);
            $categories_products = DB::select("SELECT DISTINCT p.* FROM products as p, category_products AS cp WHERE cp.id_category IN (?) and p.id = cp.id_product", [$sqlArray]);
        } else {
            $categories_products = Product::all();
        }
        return $this->buildHtmlProducts($categories_products,$lang) ;

    }

    function buildHtmlProducts($products,$lang)
    {   $sommeProd = count($products);

        $prHtml = "<input type='hidden' id='sommeProd' value='$sommeProd'>";
        if ($sommeProd === 0) {return $prHtml; }

        $viewPath = 'web.'.$lang.'.product.view';
        $imgPath = 'public/photo/products_logo/';

        foreach ($products as $product) {
            switch ($lang) {
                case 'fr': $pName = $product->product_name_fr; $viewP = 'Voir le produit'; break;
                case 'en': $pName = $product->product_name; $viewP = 'View product'; break;
                case 'de': $pName = $product->product_name_de; $viewP = 'Produkt ansehen'; break;
            }
            $routePath = route($viewPath, $product->id);
            $addCartUrl = url('add-to-cart/'. $product->id);
            $img = asset($imgPath .$product->photo);
            $prHtml .= '<li class="list_search col-sm-6 col-md-4">';
            $prHtml .= '<article class="product border-product">';
            $prHtml .= '<div class="product-figure">';
            $prHtml .= "<img src='$img' title='$pName' alt='$pName' style='width:270px;height:240px;'/>";
            $prHtml .= '<div class="product-button">';
            $prHtml .= "<a  href='$routePath' class='topmin50 absolute button button-md button-white button-ujarak'><i class='fa fa-eye'></i></a>";
            $prHtml .= "<a href='$addCartUrl' class='topmax50 absolute button button-md button-white button-ujarak' role='button'><i class='fa fa-basket'></i><i class='fa fa-shopping-cart' aria-hidden='true'></i></a>";

            $prHtml .= '</div> </div> <h5 class="product-title">';
            $prHtml .= "<a href='$routePath'> $pName</a>";
            $prHtml .= '</h5> <div class="product-price-wrap">';
             $prHtml .= "<div class='product-price'> $product->memory GB</div>";
             $prHtml .= '</div> </article></li>';
            }
        return $prHtml;
    }
}
