<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use DB;

use Validator,Redirect,Response;
use App\Product;

class GenerateQrCodeController extends Controller
{
    public function simpleQrCode() 
    {

      \QrCode::size(300)->generate('A basic example of QR code!');
       
    }    

    public function colorQrCode()
    {
       $data['Product'] = DB::table('products')
	     ->where('id', '=', "3")
       ->get();
	    
		return view('qrCode',$data);

       
    }    
    
    public function imageQrCode() 
    {

      $image = \QrCode::format('png')
               ->merge('public/image/qr/qr-code.png', 0.5, true)
               ->size(500)->errorCorrection('H')
               ->generate('A simple example of QR code!');
      return response($image)->header('Content-type','image/png');
       
    }

}