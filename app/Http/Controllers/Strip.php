<?php
namespace App\Http\Controllers;
use DB;
use App\Cart;
use App\Orders;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Strip extends Controller
{

    public function index()
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
            $amount = $cart->totalPrice;
            $intent = (new User)->createSetupIntent();
            return view('payment.strip', compact('amount', 'intent'));

        } else {
            return redirect("/cart")->with('echec', "Panier Vide");
        }
    }

    public function charge(Request $request)
    {
        $orders = Orders::where('id', '=', $request->orderId)
            ->where('customer_id', '=', Auth::user()->id)
            ->get();

        if (count($orders) > 0) {
            $cart = new Cart(session()->get('cart'));

            $stripeCharge = $request->user()->charge(
                $cart->totalPrice * 100, $request->paymentMethodId
            );

            $chargeId = $stripeCharge->id;

            if ($chargeId) {
                DB::update('update orders set payment_id = ? where id = ?',[$chargeId, $request->orderId]);
                echo 'success';
            } else {
                echo 'error';
            }

        } else {
            echo 'error_order';
        }
    }
}
