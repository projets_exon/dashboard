<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\checkbox;

class CheckboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = Sections::all();
        return view('section.index', compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('section.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=> 'required|max:255',
            'description'=> 'required|max:255'
        ]);

        $sections = new Sections([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
        ]);

        $sections->save();

        return redirect('/admin/section')->with('success', 'Create with success!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sections = Sections::find($id);

        return view('section.show', compact('sections'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sections = Sections::find($id);

        return view('section.edit', compact('sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=> 'required|max:255',
            'description'=> 'required|max:255'
			]);

        $sections = Sections::find($id);
        $sections->title = $request->get('title');
        $sections->description = $request->get('description');
        $sections->save();

        return redirect('/admin/section')->with('success', 'Update with success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sections = Sections::find($id);
        $sections->delete();

        return redirect('/admin/section')->with('success', 'Record delete!');
    }
	
	
	
	
    public function countsection() 
   {
	  
	  $sectionss = DB::table('sections')
	  ->count('id');
        return view('section.index', compact('sectionss'));
 
      
   }
}