<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Product;
use App\Product_photo;

use Session;

class Animation extends Controller
{
     public function index()
     {
     $Product = DB::table('products')
     ->get();
	
		
	return view('animation.1', compact('Product'));
    }
	
	 public function slider()
     {
     $Product = DB::table('products')
     ->get();
	
		
	return view('animation.slider', compact('Product'));
    }
	
	
     public function index2()
     {
     $Product = DB::table('products')
     ->get();
	
		
	return view('animation.2', compact('Product'));
    }
	
	
	 public function index3()
     {
     $Product = DB::table('products')
     ->get();
	
		
	return view('animation.3', compact('Product'));
    }
	
    public function index4()
     {
     $Product = DB::table('products')
     ->get();
	
		
	 return view('animation.4', compact('Product'));
     }
	 
	 public function index5()
     {
     $Product = DB::table('products')
     ->get();
	
		
	 return view('animation.5', compact('Product'));
     }
	 
	public function index6()
     {
     $Product = DB::table('products')
     ->get();
	
		
	 return view('animation.6', compact('Product'));
     }
     
     public function index7()
     {
     $Product = DB::table('products')
     ->get();
     
		
	 return view('animation.7', compact('Product'));
     }


     public function animation_caroucel()
     {
     $Product = DB::table('products')
     ->get();
	
		
	return view('animation.image-viewer-carousel.example.test', compact('Product'));
    }


    public function story()
    {
    $Product = DB::table('products')
    ->get();
    
         
    return view('animation.facebook-stories.1', compact('Product'));
   }
}