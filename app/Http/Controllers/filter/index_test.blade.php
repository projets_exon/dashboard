@extends('layouts.admin')
@section('title', 'Month')
@section('content')
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Month  ( {{ date('F Y') }} )</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="">Dashbord /</a>
<a id="pagetitlesecondelink">Month</a>
</div>
</div>

 @if(session()->get('success'))
    <div class="alert alert-success mt-3">
      {{ session()->get('success') }}  
    </div>
@endif




<div class="space"></div>
<div class="space"></div>

<div class="card row">
<div class="card-body row">
<div class="form-group col-md-12">
<label><strong>By month</strong></label>
<div class="space"></div>
<div class="space"></div>
<style>
.month_btn{padding:20px;background-color:#f1f1f1;}
.month_btn_active{padding:20px;background-color:#c0c0c0;}
</style>
@if(date('F Y'))
@else
@endif
<a class="month_btn" href="{{url('month_test')}}/{{ date('Ym', strtotime('-1 month')) }}">{{ date('F Y', strtotime('-1 month')) }}</a>
<a class="month_btn_active" href="{{url('month_test')}}/{{ date('Ym') }}">{{ date('F Y') }}</a>
<a class="month_btn" href="{{url('month_test')}}/{{ date('Ym', strtotime('+1 month')) }}">{{ date('F Y', strtotime('+1 month')) }}</a>
</div>
</div>
</div>






<table class="table table-striped mt-3">
  <thead>
    <tr>
      <th scope="col">Day</th>
      <th scope="col">Start work</th>
      <th scope="col">End work</th>
    </tr>
  </thead>
  <tbody>
@foreach($Presence_month as $post)
<tr>
<td>{{ $post->day }}</td>
<td>{{ $post->start }}</td>
<td>{{ $post->end }}</td>


    </tr>
  @endforeach
  </tbody>
</table>
    </div>

@endsection