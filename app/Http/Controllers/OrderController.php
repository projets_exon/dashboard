<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
Use App\User;
use App\Orders;
use App\Items;
use App\Purchase;
use App\Cart;



class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
          
       

        
            return view('user.cart_details.index',compact('cart'));
        }    
		}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	 
	 
	   public function store(Request $request)
    {
        $data=$request->all();
        $lastid=Orders::create($data)->id;
        if(count($request->product_name) > 0)
        {
        foreach($request->product_name as $item=>$v){
            $data2=array(
                'orders_id'=>$lastid,
                'product_name'=>$request->product_name[$item],
                'brand'=>$request->brand[$item],
                'quantity'=>$request->quantity[$item],
                'budget'=>$request->budget[$item],
                'amount'=>$request->amount[$item]
            );
        Items::insert($data2);
      }
        }
      return redirect::to('cart/details/'.$lastid.'')->with('success', 'Create with success!');
    }
	
	
    public function store2(Request $request)
    {
        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
            $item =json_encode($cart->items, JSON_FORCE_OBJECT);
            $order='C-'.time();

            Orders::create([
                'orders_number' =>$order,
                'customer_id' =>  Auth::user()->id,
                'total_amount' => $cart->totalPrice,
                'currency' =>'$',
                'items' => $item,
                'cost' => $cart->cost,
                'localisation_etats'=>1,
                'active' => 1,
                'statut' => 1,

            ]);
       


        
      return view('user.cart_details.index',compact('cart'));
        }
    }

   

    public function items($id)
    {
        $items=Items::where('orders_id','=',$id)->get();
        return view('front_page.items',compact('items'));
    }

    

}