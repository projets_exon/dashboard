<?php
namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
class CategoryController extends Controller
{
    public function index(Request $request)
    {
          
      $Category = Category::where('parent_id',0)->get();
     
      return view('category',["Category" => $Category]);
    }    
    public function subCat(Request $request)
    {
         
        $parent_id = $request->cat_id;
         
        $subcategory = Category::where('id',$parent_id)
                              ->with('subcategory')
                              ->get();
        return response()->json([
            'subcategory' => $subcategory
        ]);
    }
}