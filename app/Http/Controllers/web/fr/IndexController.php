<?php
namespace App\Http\Controllers\web\fr;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Search;
use App\Category;
use App\Country;
use App\Product;
use App\News;
use App\Contact;
use App\Pages;
use App\SEO_url;
use App\Product_photo;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Session;
use Mail;
use \Firebase\JWT\JWT;

class IndexController extends Controller
{
     public function index()
     {  
	   $Category = Category::where('parent_id',NULL)
       ->where('active', '=', "1")
       ->get();
	   
	   $Country = Country::where('parent_id',NULL)
       ->where('symbole', '=', "FRA")
       ->get();
	   foreach($Country as $post)
	   {
	    $CountryId = $post->id;
	   }
	   $City = Country::where('parent_id',$CountryId)
       ->get();

      $products = DB::table('products')
      ->get();
      return view('web.fr.index', compact('products', 'City', 'Category'));
    }
	


	public function search(Request $request)
    {
		$number = "".date('dmYhis')."";
        $this->validate($request,[
            'categoryId'=> 'required|max:255',
            'cityId'=> 'required|max:555',
            'pieces'=> 'required|max:255',
		  ]);

        $Search= new Search([
        'SearchNumber' => $number,
        'categoryId' => $request->get('categoryId'),
        'cityId' => $request->get('cityId'),
        'pieces' => $request->get('pieces'),
        ]);
        $Search->save();
        return redirect::route('web.fr.search.view', $number)->with('success', 'Create with success!')->with('success_alert', 'alert-success');
    }
   
   
	public function search_view($id)
    {
	   $search_data = DB::table('search')
		->where('search.SearchNumber', '=', $id)
	    ->get();
		foreach($search_data as $post)
	    {
		$SearchId = $post->id; 
		$SearchNumber = $post->SearchNumber; 
		$categoryId = $post->categoryId; 
		$cityId = $post->cityId; 
		$pieces = $post->pieces; 
	    }
	   
	   $products_data = DB::table('products')
	   ->join('category', 'products.category_id', '=', 'category.id_category')
		->select('products.*', 'category.name')
	   ->where('products.category_id', '=', $categoryId)
	   ->where('products.city_id', '=', $cityId)
	   ->where('products.number_pieces', '=', $pieces)
	   ->where('products.active', '=', "1")
       ->get();
	    foreach($products_data as $post)
	    {
		$ProductId = $post->id; 
	    }
		
	  
	   $Category = Category::where('parent_id',NULL)
       ->where('active', '=', "1")
       ->get();
	   
	   $Country = Country::where('parent_id',NULL)
       ->where('symbole', '=', "FRA")
       ->get();
	   foreach($Country as $post)
	   {
	    $CountryId = $post->id;
	   }
	   $City = Country::where('parent_id',$CountryId)
       ->get();

       
	  
      return view('web.fr.search.show', compact('products_data', 'City', 'Category', 'SearchId', 'SearchNumber', 'categoryId', 'cityId', 'pieces'));
    }
	
    public function location_update(Request $request, $id) 
    {
	   $this->validate($request,[]);
      
	    $products_id = $request->input('products_id');
	    $name_url = $request->input('name_url');
		
       DB::update('update search set products_id = ? where id = ?',[$products_id,$id]);
	   return redirect::route('web.fr.location.view', $request->input('name_url'))->with('success', 'Create with success!')->with('success_alert', 'alert-success');
    }
	
	
	public function location_view($id)
    {
	   
	   $products_data = DB::table('products')
	   	->where('products.name_url', '=', $id)
	   ->join('category', 'products.category_id', '=', 'category.id_category')
		->select('products.*', 'category.name')
	   ->where('products.active', '=', "1")
       ->get();
	    foreach($products_data as $post)
	    {
		$ProductId = $post->id; 
	    }
		
	  
	   $Category = Category::where('parent_id',NULL)
       ->where('active', '=', "1")
       ->get();
	   
	   $Country = Country::where('parent_id',NULL)
       ->where('symbole', '=', "FRA")
       ->get();
	   foreach($Country as $post)
	   {
	    $CountryId = $post->id;
	   }
	   $City = Country::where('parent_id',$CountryId)
       ->get();

       
	  
      return view('web.fr.search.location', compact('products_data', 'City', 'Category'));
    }

    public function services()
   {
    return view('web.fr.services.index');
   }


public function about()
{
    $products = DB::table('products')
    ->get();

    $productsl4 = DB::table('products')
    ->limit(4)
    ->get();

    $newsl3 = DB::table('news')
    ->limit(4)
    ->get();

    $Category = DB::table('category')
    ->get();



        $Pages = DB::table('pages')
        ->join('seo_url', 'pages.name', '=', 'seo_url.page')
        ->select('pages.id', 'pages.name_fr', 'seo_url.url')
        ->where('seo_url.lang', '=', "fr")
        ->get();


    return view('web.fr.about-us.index', compact('products', 'productsl4', 'newsl3', 'Category', 'Pages'));
}


public function blog()
{
$products = DB::table('products')
->get();

$productsl4 = DB::table('products')
->limit(4)
->get();

$news = DB::table('news')
->where('active', '=', "1")
->get();

$Category = DB::table('category')
->get();



     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "fr")
    ->get();


return view('web.fr.blog.index', compact('products', 'productsl4', 'news', 'Category', 'Pages'));
}








public function contact()
{
$products = DB::table('products')
->get();

$productsl4 = DB::table('products')
->limit(4)
->get();

$news = DB::table('news')
->limit(4)
->get();

$Category = DB::table('category')
->get();


     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "fr")
    ->get();


return view('web.fr.contact-us.index', compact('products', 'productsl4', 'news', 'Category', 'Pages'));
}



public function contact_save(Request $request)
{
    $this->validate($request,[
        'firstname'=> 'required|max:255',
        'lastname'=> 'required|max:255',
        'email'=> 'required|max:255',
        'message'=> 'required|max:2255'
        ]);

  $Contact= new Contact([
    'firstname' => $request->get('firstname'),
    'lastname' => $request->get('lastname'),
    'email' => $request->get('email'),
    'message' => $request->get('message'),
    'active' => "1",
    ]);
  $Contact->save();
  return redirect::back()->with('success', 'Votre message a été envoyé avec succès!');
}





public function product_show($id)
{

$products = Product::find($id);

$Product_photo = DB::table('products_photo')
->where('products_id', '=', "".$id."")
->get();

$productsl4 = DB::table('products')
->limit(4)
->get();

$news = DB::table('news')
->limit(4)
->get();

$Category = DB::table('category')
->get();

     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "fr")
    ->get();


return view('web.fr.product.show', compact('products', 'productsl4', 'news', 'Category', 'Pages', 'Product_photo'));
}







public function category_show($id)
{

$Category = Category::find($id);


         $category_product = DB::table('category')
		->join('category_products', 'category.id', '=', 'category_products.id_category')
		->join('products', 'category_products.id_product', '=', 'products.id')
		->select('category.id', 'category.name', 'products.product_name_fr', 'products.photo_sec', 'products.photo_alt_fr', 'products.photo', 'products.memory', 'category.parent_id', 'category_products.id_product')
		->where('category.id', '=', $id)
	    ->get();


return view('web.fr.category.show', compact('category_product', 'Category'));
}


public function blog_show($id)
{

$news = News::find($id);


$productsl4 = DB::table('products')
->limit(4)
->get();


$Category = DB::table('category')
->get();


     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "fr")
    ->get();


return view('web.fr.blog.show', compact('productsl4', 'news', 'Category', 'Pages'));
}

public function forgot_fr()
{
    return view('web.fr.forgot-password.forgot');

}

public function  forgot_en()
{
    return view('web.en.forgot-password.forgot');

}
public function  verify_view_fr()
{
    return view('web.fr.verify.verify');

}
public function  verify_view_en()
{
    return view('web.en.verify.verify');

}
public function  verify_view_de()
{
    return view('web.de.verify.verify');

}

public function reset_fr()
{
    return view('web.fr.reset-password.reset');
}

 
    public function forgot_password(Request $request,$lang)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);

    $key = env('JWT_KEY');
    $issued_at = time();
    $expiration_time = $issued_at + (30 * 30) ; // valid for 1 hour

    $email=$request->get('email');
    $user = DB::select('select id from users where email="'.$email.'"');
    $token = array(
        "iat" => $issued_at,
        "exp" => $expiration_time,
        "data" => array(
            "id" => $user[0]->id
        )
     );
     $jwt = JWT::encode($token, $key);

        
        DB::update('update users set remember_token ="'.$jwt.'" where id = "'.$user[0]->id.'"');
        $request->session()->put('token', $jwt);
        
        Mail::send('web.'.$lang.'.verify.verify', ['token' => $jwt], function($message) use($request){
            $message->from('bouzeziahmed5@gmail.com');
            $message->to($request->get('email'));
            $message->subject('Reset Password Notification');
        }); 
        if($lang == 'en'){
            $message='We have e-mailed you to reset your password !';    
        }else if($lang == 'fr'){
            $message='Nous vous avons envoyé un e-mail pour réinitialiser votre mot de passe.';
        }else if($lang == 'de'){
            $message='Wir haben Ihnen eine E-Mail geschickt, um Ihr Passwort zurückzusetzen !';  
        }else{
            $message='';
        }
        return view('web.'.$lang.'.forgot-password.forgot', compact('message'));
    }




    public function reset_pass(Request $request,$lang){
        $jwt = $request->session()->get('token');
        $key = env('JWT_KEY');
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        if($lang == 'en'){
            $message='password has been successfully changed ';    
        }else if($lang == 'fr'){
            $message='le mot de passe a été changé avec succès ';
        }else if($lang == 'de'){
            $message='Passwort wurde erfolgreich geändert ';  
        }else{
            $message='';
        }
        if($decoded){
                $id = $decoded->data->id;
                $password=Hash::make($request->get('password'));
                DB::update('update users set password ="'.$password.'" where id = "'.$id.'"');
                
        }
        return view('web.'.$lang.'.reset-password.reset', compact('message'));
        

    }

}
