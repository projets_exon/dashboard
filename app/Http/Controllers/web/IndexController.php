<?php
namespace App\Http\Controllers\web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Language;
use Session;

class IndexController extends Controller
{
     public function index()
     {
	   $languages = DB::table('language')
       ->where('bydefault', '=', "1")
       ->get();
	          foreach($languages as $post)
	          {
	          $lang_name = $post->symbole;
	          }
       return redirect('/'.$lang_name.'');
     }
}
