<?php
namespace App\Http\Controllers\web\en;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Category;
use App\Product;
use App\News;

use App\Pages;
use App\SEO_url;
use App\Product_photo;
use App\Contact;

use Session;




class IndexController extends Controller
{
     public function index()
     {

     $products = DB::table('products')
     ->get();

     $productsl4 = DB::table('products')
     ->limit(4)
     ->get();

     $newsl3 = DB::table('news')
     ->where('active', '=', "1")
     ->get();

     $Category = DB::table('category')
     ->get();



          $Pages = DB::table('pages')
		->join('seo_url', 'pages.name', '=', 'seo_url.page')
          ->select('pages.id', 'pages.name_fr', 'seo_url.url')
          ->where('seo_url.lang', '=', "de")
         ->get();


	return view('web.en.index', compact('products', 'productsl4', 'newsl3', 'Category', 'Pages'));
}





public function about()
{
$products = DB::table('products')
->get();

$productsl4 = DB::table('products')
->limit(4)
->get();

$newsl3 = DB::table('news')
->limit(4)
->get();

$Category = DB::table('category')
->get();



     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "de")
    ->get();


return view('web.en.about-us.index', compact('products', 'productsl4', 'newsl3', 'Category', 'Pages'));
}


public function blog()
{
$products = DB::table('products')
->get();

$productsl4 = DB::table('products')
->limit(4)
->get();

$news = DB::table('news')
->where('active', '=', "1")
->get();

$Category = DB::table('category')
->get();



     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "de")
    ->get();


return view('web.en.blog.index', compact('products', 'productsl4', 'news', 'Category', 'Pages'));
}




public function shop()
{
$products = DB::table('products')
->get();

$productsl4 = DB::table('products')
->limit(4)
->get();

$news = DB::table('news')
->limit(4)
->get();

$Category = DB::table('category')
->get();



     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "de")
    ->get();


return view('web.en.shop.index', compact('products', 'productsl4', 'news', 'Category', 'Pages'));
}


public function contact()
{
$products = DB::table('products')
->get();

$productsl4 = DB::table('products')
->limit(4)
->get();

$news = DB::table('news')
->limit(4)
->get();

$Category = DB::table('category')
->get();



     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "de")
    ->get();


return view('web.en.contact-us.index', compact('products', 'productsl4', 'news', 'Category', 'Pages'));
}



public function contact_save(Request $request)
{
    $this->validate($request,[
        'firstname'=> 'required|max:255',
        'lastname'=> 'required|max:255',
        'email'=> 'required|max:255',
        'message'=> 'required|max:2255'
        ]);

  $Contact= new Contact([
    'firstname' => $request->get('firstname'),
    'lastname' => $request->get('lastname'),
    'email' => $request->get('email'),
    'message' => $request->get('message'),
    'active' => "1",
    ]);
  $Contact->save();
  return redirect::back()->with('success', 'Votre message a été envoyé avec succès!');
}







public function product_show($id)
{

$products = Product::find($id);

$Product_photo = DB::table('products_photo')
->where('products_id', '=', "".$id."")
->get();

$productsl4 = DB::table('products')
->limit(4)
->get();

$news = DB::table('news')
->limit(4)
->get();

$Category = DB::table('category')
->get();


     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "fr")
    ->get();


return view('web.en.product.show', compact('products', 'productsl4', 'news', 'Category', 'Pages', 'Product_photo'));
}



public function category_show($id)
{

$Category = Category::find($id);


         $category_product = DB::table('category')
		->join('category_products', 'category.id', '=', 'category_products.id_category')
		->join('products', 'category_products.id_product', '=', 'products.id')
		->select('category.id', 'category.name', 'products.product_name', 'products.photo', 'products.photo_sec', 'products.photo_alt', 'products.memory', 'category.parent_id', 'category_products.id_product')
		->where('category.id', '=', $id)
	    ->get();


return view('web.en.category.show', compact('category_product', 'Category'));
}

public function blog_show($id)
{

$news = News::find($id);

$productsl4 = DB::table('products')
->limit(4)
->get();


$Category = DB::table('category')
->get();


     $Pages = DB::table('pages')
     ->join('seo_url', 'pages.name', '=', 'seo_url.page')
     ->select('pages.id', 'pages.name_fr', 'seo_url.url')
     ->where('seo_url.lang', '=', "de")
    ->get();


return view('web.en.blog.show', compact('productsl4', 'news', 'Category', 'Pages'));
}



public function forgot_en()
{
    return view('web.en.forgot-password.forgot');

}

public function reset_en()
{
    return view('web.en.reset-password.reset');

}


}

