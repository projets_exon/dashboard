<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\postRoom;
use App\Product;

class MemberController extends Controller
{
    public function index()
    {
        $postRoom = PostRoom::all();
        return view('checkbox.index', compact('postRoom'));
    }
public function store(Request $request)
{
 $sundays = $request->input('checkbox-sunday');

    $sundaysArray = array();

    foreach($sundays as $sunday){
       $sundaysArray[] = $sunday;
    }


        $postRoom = PostRoom::all();
$postRoom->day_sunday = $request->get('sundays');

    $postRoom->save();

    return redirect('checkbox');
	}

public function create()
{
    return view('checkbox.create');
}
}