<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use App\Cart;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
class Paypal extends Controller
{
    private  $secret;

    private $cliendId;

    private $apiContext ;
    public function __construct(){

        $this->secret='ATtEUudHQrozBY95yg7Vs3Kh0_282Q1FHTxvUl7Ab36nyedV9iWB9EdtWP9m6_VCuuJhK7V3KLARqFA_';
        $this->cliendId='EI_eT71uE5NxOPJZ2UryeRLMQWx92PW50hW9GIqul9N7wUf0XL-H1s0V-mcHLmp0K579KDw_NjT6Yf3C';
        $this->apiContext=new ApiContext(new OAuthTokenCredential($this->secret,$this->cliendId));

    }

    public function paypal(Request $request)
    {

        if (session()->has('cart')) {
            $cart = new Cart(session()->get('cart'));
            $itemtab=[];
            $i=0;
            foreach ( $cart->items as $product) {

                $item=new Item();
                $item->setName($product["title"])
                      ->setCurrency('USD')
                      ->setQuantity($product["qty"])
                     ->setPrice($product["price"]);
                $itemtab[$i]=$item;
                $i++;

            }
            $item=new Item();
            $item->setName('cost')
                 ->setCurrency('USD')
                 ->setQuantity(1)
                 ->setPrice($cart->cost);
            $itemtab[$i+1]=$item;


            $payer = new Payer();

            $payer->setPaymentMethod("paypal");

            $itemlist=new ItemList();
            $itemlist->setItems($itemtab);
            $amount =new Amount();
            $amount->setCurrency('USD')
                ->setTotal($cart->totalPrice+$cart->cost);
            $transaction=new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemlist)
                ->setDescription("Votre paiement salon");
            $redirectUrls = new RedirectUrls();
            $redirectUrls->setReturnUrl(url('status'))
                ->setCancelUrl(url('canceled'));

            $payment = new Payment();
            $payment->setIntent('sale')
                ->setPayer($payer)
                ->setTransactions(array($transaction))
                ->setRedirectUrls($redirectUrls);
            try {

                $payment->create($this->apiContext);


                $link= $payment->getApprovalLink();
                return redirect($link);

            }
            catch (PayPalConnectionException $ex) {

                echo $ex->getData();
            }
        } else {
            return redirect("panier")->with('echec', "Panier vide");
        }
    }

    public function status(Request $request)
    {

        $paymentId=$request->get('paymentId');
        $payment=Payment::get($paymentId,$this->apiContext);
        $exuction=new PaymentExecution();
        $exuction->setPayerId($request->input('PayerID'));
        $result=$payment->execute($exuction,$this->apiContext);
        if($result->getState()=='approved')
        {
            $cart = new Cart(session()->get('cart'));
           // $ordre  =new OrdreController;
            //$ordre ->store(1);
           $regulation  =new RegulationController;
            $regulation ->store($paymentId,"Paypal",1,$cart->id_ordre);
            session()->forget('cart');

       return redirect("/cart")->with('echec', "Paiement effectue avec succès");
        }

            die("");
        echo "Paiement echouer";
        die($result);

    }

    public function canceled()
    {

        return redirect("/cart")->with('echec', "Paiement Annuler");


    }

}
