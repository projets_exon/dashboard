<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator,Redirect,Response;
Use App\User;
Use App\History;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class AdminLoginController extends Controller
{

    public function index()
    {
    return view('admin.login');
    }


    public function registration()
    {
    return view('admin.registration');
    }



    public function postLogin(Request $request)
    {
        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => '1']))
        {
          $user=Auth::user();
          if($user->role=='0')
          {
          return redirect()->back()->with('success', 'Bienvenue');
          }
          elseif($user->role!='0')
          {
		  return redirect(''.app()->getLocale().'/admin/home');
          }

        }
         return redirect()->back()->with('success', 'Oppes! You have entered invalid credentials');
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect(''.app()->getLocale().'/admin');
    }
}
