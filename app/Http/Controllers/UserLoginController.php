<?php
namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Category;
use App\History;
use App\Notifications;

class UserLoginController extends Controller
{

    public function index()
    {
	  return view('web.fr.login.index');
    }
     
    public function index_en()
    {
      return view('web.en.login.index');
    }
    
    public function index_de()
    {
      return view('web.de.login.index');
    }


    public function registration_fr()
    {
      return view('web.fr.registration.index');
    }
    
    public function postLogin(Request $request)
    {
        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => '1'])) 
        {

        $notifications = DB::table('notifications')
       ->get();
          $user=Auth::user();
          $ip=$request->getClientIp();
       
          $history = new History;
          $history->user_id=$user->id;
          $history->last_login_ip = $ip;
          $history->country= 'Tunisie';
          $history->city= 'Tunis';
          $history->history_of= 'login';
          $history->save();
          if($user->role=='0')
          {
          return redirect()->back()->with('success', 'Bienvenue');
          }
          elseif($user->role!='0')
          {
          return redirect()->intended('admin/home');
          }
          
        }
         return redirect()->back()->with('success', 'Oppes! You have entered invalid credentials');
    }

    public function logout() {
        Session::flush();
        Auth::logout();
    return redirect()->back()->with('success', '');
    }
	
	


    public function postRegistration(Request $request)
    {  
          request()->validate([
           'status' => 'required',
          'role' => 'required',
          'birthday' => 'required',
          'gender' => 'required',
          'firstname' => 'required',
          'lastname' => 'required',
          'image' => 'required',
          'country' => 'required',
          'city' => 'required',
          'email' => 'required|email|unique:users',
          'password' => 'required|min:6',
          'block_description' => 'required',
          'year' => 'required',


        ]);
        
        $data = $request->all();

        $check = $this->create($data);
      
        return Redirect::to("cart/dashboard")->withSuccess('Great! You have Successfully loggedin');
    }
	
	
	










    public function store(Request $request)
    {
        $this->validate($request,[
          'firstname' => 'required',
          'lastname' => 'required',
          'email' => 'required|email|unique:users',
          'password' => 'required|min:6',
		  ]);

		
      $user= new User([
        'firstname' => $request->get('firstname'),
        'lastname' => $request->get('lastname'),
        'email' => $request->get('email'),
        'password' => Hash::make($request['password']),

        ]);
		
      $user->save();

      return redirect::route('user.dashboard')->with('success', 'Create with success!');
    }


	
	
	
	
	
    public function postRegistration2(Request $request)
    {  
        request()->validate([
          'status' => 'required',
          'role' => 'required',
          'birthday' => 'required',
		  


        ]);
        
        $data = $request->all();

        $check = $this->create($data);
      
        return Redirect::to("user/dashboard")->withSuccess('Great! You have Successfully loggedin');
    }
    
   


    public function create(array $data)
    {
		
		
      return User::create([
          'status' => 'required',
          'role' => 'required',
          'birthday' => 'required',
          'gender' => 'required',
          'firstname' => 'required',
          'lastname' => 'required',
		  'image' => 'required',
          'country' => 'required',
          'city' => '',
          'email' => 'required|email|unique:users',
          'password' => 'required|min:6',
          'block_description' => 'required',





      ]);
    }
    

    

}