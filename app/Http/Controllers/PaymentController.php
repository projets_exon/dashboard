<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Cart;
use App\Category;

use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas['Category'] = Category::where('parent_id',0)
		->where('parent_id', '=', "0")
        ->get();
        

        // check our price is calculated through view composer

        return view('user.cart_details.index',$datas);
    }

    
}
