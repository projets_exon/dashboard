<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\book_page_title;
use Redirect,Response;

class PagesTitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function index()
    {
        //
        $data['book_pages_titles'] = Book_page_title::orderBy('id','desc')->paginate(8);
   
        return view('contact_form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $postID = $request->post_id;
        $post   =   Book_page_title::updateOrCreate(['id' => $postID],
                    ['title' => $request->title, 'date_at' => $request->date_at]);
    
        return Response::json($post);
    }
	
	public function edit($id)
    {
        //
        $where = array('id' => $id);
        $post  = Book_page_title::where($where)->first();
 
        return Response::json($post);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	 public function destroy($id)
    {
        //
        $post = Book_page_title::where('id',$id)->delete();
   
        return Response::json($post);
    }
}