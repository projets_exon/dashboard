<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\checkbox;
use App\Product;

class CheckboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $checkbox = Checkbox::all();
        return view('checkbox.index', compact('checkbox'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                $product = Product::all();
                return view('checkbox.create', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
	
 	
    	
	
public function store(Request $request) 
{
$checkbox = new Checkbox();
$checkbox->title = $request->has('title'); // Will set $article->title to true/false based on whether title exists in your input
// ...
$checkbox->save();

    return redirect('checkbox');
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $checkbox = Checkbox::find($id);

        return view('checkbox.show', compact('checkbox'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $checkbox = Checkbox::find($id);

        return view('section.edit', compact('checkbox'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=> 'required|max:255',
            'description'=> 'required|max:255'
			]);

        $checkbox = Checkbox::find($id);
        $checkbox->title = $request->get('title');
        $checkbox->description = $request->get('description');
        $checkbox->save();

        return redirect('checkbox.index')->with('success', 'Update with success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkbox = Checkbox::find($id);
        $checkbox->delete();

        return redirect('checkbox')->with('success', 'Record delete!');
    }
	
	
	
	
    public function countsection() 
   {
	  
	  $checkbox = DB::table('checkbox')
	  ->count('id');
        return view('checkbox.index', compact('checkbox'));
 
      
   }
}