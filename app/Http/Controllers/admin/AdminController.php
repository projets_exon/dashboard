<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Session;
use App\Circuit;
use App\Point;
use Response;

class AdminController extends Controller
{

    public function __construct()
    {
    $this->middleware('auth');
    }

	public function index()
    {
	 $data['User'] = DB::table('users')
	->where('role', '=', '1')
	->get();
	return view('admin.adminlist.index',$data);
	}
	
	public function destroy(Request $request, $id)
    {
      $this->validate($request,[]);
      $active = $request->input('active');
      DB::update('update users set active = "0"',[$id]);
      return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');
    }
	
	public function update_layouts(Request $request) 
   {
    $header = $request->input('header');
    $sidebar = $request->input('sidebar');
    $body = $request->input('body');
    

      DB::update('update users set header = ?, sidebar = ?, body = ? where id = ?',[$header,$sidebar,$body,ucfirst(Auth()->user()->id)]);
      return redirect()->back()->with('success layouts', 'Update with success!');

   }
	
	public function activate(Request $request, $id)
    {
      $this->validate($request,[]);
      $active = $request->input('active');
      DB::update('update users set active = "1"',[$id]);
      return redirect()->back()->with('success', 'Admin is actived !')->with('success_alert', 'alert-success');

    }
	
	public function desactive(Request $request, $id)
    {
      $this->validate($request,[]);
      $active = $request->input('active');
      DB::update('update users set active = "0"',[$id]);
      return redirect()->back()->with('success', 'Admin is desactived !')->with('success_alert', 'alert-danger');

    }
}
