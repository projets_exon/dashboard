<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Session;
use App\Circuit;
use App\Point;
use App\Student;
use Response;

class CircuitController extends Controller
{
	
    public function __construct()
    {
    $this->middleware('auth');
    }

	public function index()
    {
	$data['Circuit'] = Circuit::all();
	return view('admin.circuit.index',$data);
	}


	public function create()
    {
	$rowReplace = 0;
    return view('admin.circuit.create', compact("rowReplace"));
	}


    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required|max:255',
			'row.*.image.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',

            'row.*.title' => 'required|max:255',
            'row.*.longitude' => 'required|max:255',
            'row.*.lattitude' => 'required|max:255',
        ],[
            'title.required' => 'Circuit title is required.',
            'row.*.title.required' => 'Point title is required.',
            'row.*.longitude.required' => 'Point longitude is required.',
            'row.*.lattitude.required' => 'Point lattitude is required.',
        ]);
        $storeData = new Circuit;
        $storeData = $storeData->storeData($request);
        return redirect()->route('circuit.index', app()->getLocale());

    }



	public function edit($id)
    {
	  $circuits = DB::table('circuit')
	  ->where('circuit.idCircuit', '=', $id)
	  ->get();

	  $circuit = DB::select('select * from circuit where idCircuit='.$id.'');
       return view('admin.circuit.edit', compact('circuit'));
	}

    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'title' =>'required',
            'active' =>'required'
        ]);

        $title = $request->input('title');
        $active = $request->input('active');

       DB::update('update circuit set title = ?, active = ? where idCircuit = ?',[$title,$active,$id]);
         return redirect()->back()->with('success', 'Create with success!');

    }

	public function destroy(Request $request, $id)
    {
         $this->validate($request,[]);

         $active = $request->input('active');

         DB::update('update circuit set active = "0"',[$id]);
         return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');

    }

	public function activate(Request $request, $id)
    {
         $this->validate($request,[]);

         $active = $request->input('active');
         DB::update('update circuit set active = "0"');
         DB::update('update circuit set active = "1" where idCircuit = ?',[$id]);
         return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');

    }




}
