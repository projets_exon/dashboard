<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Session;
use App\Circuit;
use App\Point;
use App\Quiz;
use Response;

class QuizController extends Controller
{

    public function __construct()
    {
    $this->middleware('auth');
    }

	public function index()
    {
	 $data['Quiz'] = Quiz::all();

	return view('admin.quiz.index',$data);
	}

    public function create()
    {
	 $circuit = Circuit::where('active',1)
	 ->get();
	 foreach($circuit as $row)
	 {
     $id_circuit = $row->idCircuit; 
	 }
	 
	 $point = DB::table('point')
	 ->where('circuit','=',"".$id_circuit."")
     ->get();
	  
    return view('admin.quiz.create', compact('circuit', 'point'));
	}

	public function store(Request $request)
    {
        $this->validate($request,[
            'question'=> 'required|max:255',
            'active'=> 'required|max:255',
            'proposition_1'=> 'required|max:255',
            'proposition_2'=> 'required|max:255',
            'proposition_3'=> 'required|max:255',
            'proposition_4'=> 'required|max:255',
            'point'=> 'required|max:255',
            'reponse'=> 'required|max:255'
		  ]);
 
          $Quiz= new Quiz([
            'question' => $request->get('question'),
            'active' => $request->get('active'),
            'proposition_1' => $request->get('proposition_1'),
            'proposition_2' => $request->get('proposition_2'),
            'proposition_3' => $request->get('proposition_3'),
            'proposition_4' => $request->get('proposition_4'),
            'point' => $request->get('point'),
            'reponse' => $request->get('reponse'),
        ]);
		
      $Quiz->save();

      return redirect::route('quiz.index', app()->getLocale())->with('success', 'Create with success!')->with('success_alert', 'alert-success');
    }


    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'question' =>'required',
            'active' =>'required'
        ]);

        $question = $request->input('question');
        $active = $request->input('active');
        $proposition_1 = $request->input('proposition_1');
        $proposition_2 = $request->input('proposition_2');
        $proposition_3 = $request->input('proposition_3');
        $proposition_4 = $request->input('proposition_4');

       DB::update('update quiz set question = ?, active = ?, proposition_1 = ?, proposition_2 = ?, proposition_3 = ?, proposition_4 = ? where idQuiz = ?',[$question,$active,$proposition_1,$proposition_2,$proposition_3,$proposition_4,$id]);
         return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');

    }

	public function destroy(Request $request, $id)
    {
         $this->validate($request,[]);

         $active = $request->input('active');

         DB::update('update quiz set active = "0" where idQuiz = ?',[$id]);
         return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');
    }
	
	
    public function activate(Request $request, $id, $point)
    {
         $this->validate($request,[]);
	
         $active = $request->input('active');
         DB::update('update quiz set active = "0" where point = "'.$point.'"');
         DB::update('update quiz set active = "1" where idQuiz = ? AND point = "'.$point.'"',[$id]);
         return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');

    }


}
