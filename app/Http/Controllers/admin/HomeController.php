<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Session;

class HomeController extends Controller 
{

    public function __construct()
    {
    $this->middleware('auth');
    }


    public function dashboard()
    {
     return view('admin.home');
    }
}