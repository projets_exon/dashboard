<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Session;
use App\Language;
use Response;

class LangueController extends Controller
{

    public function __construct()
    {
    $this->middleware('auth');
    }

	public function index()
    {
	 $data['Language'] = DB::table('language')
	->get();
	return view('admin.langue.index',$data);
	}
	
	public function destroy(Request $request, $id)
    {
      $this->validate($request,[]);
      $active = $request->input('active');
      DB::update('update language set active = "0" where idLanguage = ?',[$id]);
      return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');

    }
	
	
	public function activate(Request $request, $id)
    {
      $this->validate($request,[]);
      $active = $request->input('active');
      DB::update('update language set active = "1" where idLanguage = ?',[$id]);
      return redirect()->back()->with('success', 'Language is actived !')->with('success_alert', 'alert-success');

    }
	
	public function desactive(Request $request, $id)
    {
      $this->validate($request,[]);
      $active = $request->input('active');
      DB::update('update language set active = "0" where idLanguage = ?',[$id]);
      return redirect()->back()->with('success', 'Language is desactived !')->with('success_alert', 'alert-danger');

    }
}
