<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Session;
use App\Circuit;
use App\Point;
use Response;

class PointController extends Controller
{

    public function __construct()
    {
    $this->middleware('auth');
    }

	public function index()
    {
	 $data['Point'] = Point::all();
	return view('admin.point.index',$data);
	}



	public function edit($id)
    {
	  $point = DB::table('point')
	  ->where('point.idPoint', '=', $id)
	  ->get();

	  $point = DB::select('select * from point where idPoint='.$id.'');
       return view('admin.point.edit', compact('point'));
	}

    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'title' =>'required',
            'longitude' =>'required',
            'lattitude' =>'required',
            'active' =>'required'
        ]);

        $title = $request->input('title');
        $longitude = $request->input('longitude');
        $lattitude = $request->input('lattitude');
        $active = $request->input('active');

       DB::update('update point set title = ?, longitude = ?, lattitude = ?, active = ? where idPoint = ?',[$title,$longitude,$lattitude,$active,$id]);
         return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');

    }
	
	
	
	public function edittype(Request $request, $id)
    {
         $this->validate($request,[
            'type' =>'required',
			'image' =>'required',
            'image.*' => 'mimes:jpeg,png,jpg,gif,svg|max:7048'
        ]);
		 $type = $request->input('type');

        if($request->input('type')=='1')
        {
	        $image = $request->file('image');
            $inputValue['image'] = $image->getClientOriginalName();
            $image->move(public_path('file_point/image'),
		    $image->getClientOriginalName());	
        }
        elseif($request->input('type')=='2')
        {
	        $image = $request->file('image');
            $inputValue['image'] = $image->getClientOriginalName();
            $image->move(public_path('file_point/video'),
		    $image->getClientOriginalName());
        }
		elseif($request->input('type')=='3')
        {
	       $inputValue['image'] = $request->input('image');
        }
		
        DB::update('update point set type =?, image = ? where idPoint = ?',[$type,$inputValue['image'],$id]);
        return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');

    }


   
   
	public function destroy(Request $request, $id)
    {
         $this->validate($request,[]);

         $active = $request->input('active');

         DB::update('update point set active = "0" where idPoint = ?',[$id]);
         return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');

    }
    public function activate(Request $request, $id)
    {
         $this->validate($request,[]);

         $active = $request->input('active');

         DB::update('update point set active = "1" where idPoint = ?',[$id]);
         return redirect()->back()->with('success', 'Update with success!')->with('success_alert', 'alert-info');

    }


}
