<?php
namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
class Circuit extends Model
{
protected $table = 'circuit';
protected $fillable = ['title','active'];

public function storeData($request){
    	$circuit = Circuit::create([
            'title'=>request('title')
			]);
        $point = $request->row;
        $insert_ary = [];
        foreach ($point as $key => $value) {
            $insert_ary[$key] = $value;
            $insert_ary[$key]['circuit'] = $circuit->id;
        }
		
        DB::table('point')->insert($insert_ary);
}
	
public function playes()
{
return $this->hasMany('App\Playes', 'circuit');
}
public function circuit_point()
{
return $this->hasMany('App\Point', 'circuit', 'idCircuit');
}
}