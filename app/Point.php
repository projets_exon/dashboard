<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Point extends Model
{
protected $table = 'point';
protected $fillable = ['title','description','longitude','lattitude','active','type','video','image','circuit'];

public function circuit_point()
{
return $this->belongsTo(Circuit::class); 
}

public function point_play()
{
return $this->hasMany('App\Playes', 'current');
}
}