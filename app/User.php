<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Billable;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['status', 'role', 'birthday', 'gender', 'firstname', 'lastname', 'siren_number', 'vat_number', 'image' , 'country', 'city', 'tel', 'mobile', 'address', 'postcode', 'email', 'password', 'block_description', 'year', 'month_number','header','sidebar','body', 'parent_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
public function user_language()
{
return $this->belongsTo(Language::class); 
}
	
public function user_play()
{
return $this->hasMany('App\Playes', 'user');
}

}
