<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Language extends Model
{
protected $table = 'language';
protected $fillable = ['labelLang ','symbole','photo','active','bydefault'];


public function user_language()
{
return $this->hasMany('App\user', 'languages');
}
}

