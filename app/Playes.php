<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Playes extends Model
{
protected $table = 'playes';
protected $fillable = ['user','circuit','current'];


public function circuit_play()
{
return $this->belongsTo(Circuit::class); 
}
public function user_play()
{
return $this->belongsTo(User::class); 
}
public function point_play()
{
return $this->belongsTo(Point::class); 
}
}