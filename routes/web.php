<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
  'prefix' => '{locale}',
  'where' => ['locale' => '[a-zA-Z]{2}'],
  'middleware' => 'setlocale'], function() {

Route::get('admin', 'AdminLoginController@index')->name('login');
Route::post('admin', 'AdminLoginController@postLogin')->name('login.post');
Route::post('logout', 'AdminLoginController@logout')->name('logout');
Route::get('admin/home', 'admin\HomeController@dashboard')->name('home');

//route Circuit
Route::get('admin/circuit', 'admin\CircuitController@index')->name('circuit.index');
Route::get('admin/circuit/create', 'admin\CircuitController@create')->name('circuit.create');
Route::post('admin/circuit/store', 'admin\CircuitController@store')->name('circuit.store');
Route::get('admin/circuit/{id}/update_pub_active', 'admin\CircuitController@update_pub_active')->name('circuit.active');
Route::get('admin/circuit/{id}/update_pub_blocked', 'admin\CircuitController@update_pub_blocked')->name('circuit.blocked');
Route::get('admin/circuit/{id}/edit', 'admin\CircuitController@edit')->name('circuit.edit');
//end route Circuit

//route Point
Route::get('admin/point', 'admin\PointController@index')->name('point.index');
//end route Point

//route Quiz
Route::get('admin/quiz', 'admin\QuizController@index')->name('quiz.index');
Route::get('admin/quiz/create', 'admin\QuizController@create')->name('quiz.create');
Route::post('admin/quiz/store', 'admin\QuizController@store')->name('quiz.store');
//end route Quiz

//route User
Route::get('admin/user', 'admin\UserController@index')->name('user.index');
//end route User

//route Admin
Route::get('admin/apadm', 'admin\AdminController@index')->name('admin.index');
//end route Admin

//route Admin
Route::get('admin/langue', 'admin\LangueController@index')->name('langue.index');
//end route Admin
});
//Route::get('', 'HomeController@index')->name('home');

Route::patch('admin/update_layouts','admin\AdminController@update_layouts')->name('admin.update_layouts');

Route::patch('admin/circuit/{id}/destroy', 'admin\CircuitController@destroy')->name('circuit.destroy');
Route::patch('admin/circuit/{id}/update', 'admin\CircuitController@update')->name('circuit.update');
Route::get('admin/circuit/{id}/destroy', 'admin\CircuitController@destroy')->name('circuit.destroy');
Route::get('admin/circuit/{id}/activate', 'admin\CircuitController@activate')->name('circuit.activate');

Route::patch('admin/point/{id}/destroy', 'admin\PointController@destroy')->name('point.destroy');
Route::get('admin/point/{id}/destroy', 'admin\PointController@destroy')->name('point.destroy');
Route::patch('admin/point/{id}/update', 'admin\PointController@update')->name('point.update');
Route::get('admin/point/{id}/activate', 'admin\PointController@activate')->name('point.activate');
Route::patch('admin/point/{id}/edittype', 'admin\PointController@edittype')->name('point.type');

Route::patch('admin/quiz/{id}/destroy', 'admin\QuizController@destroy')->name('quiz.destroy');
Route::patch('admin/quiz/{id}/update', 'admin\QuizController@update')->name('quiz.update');
Route::get('admin/quiz/{id}/destroy', 'admin\QuizController@destroy')->name('quiz.destroy');
Route::get('admin/quiz/activate/{id}/{point}', 'admin\QuizController@activate')->name('quiz.activate');

Route::patch('admin/user/{id}/destroy', 'admin\UserController@destroy')->name('user.destroy');
Route::get('admin/user/{id}/activate', 'admin\UserController@activate')->name('user.activate');
Route::get('admin/user/{id}/desactivate', 'admin\UserController@desactive')->name('user.desactive');

Route::patch('admin/apadm/{id}/destroy', 'admin\AdminController@destroy')->name('admin.destroy');
Route::get('admin/apadm/{id}/activate', 'admin\AdminController@activate')->name('admin.activate');
Route::get('admin/apadm/{id}/desactivate', 'admin\AdminController@desactive')->name('admin.desactive');

Route::patch('admin/langue/{id}/destroy', 'admin\LangueController@destroy')->name('langue.destroy');
Route::get('admin/langue/{id}/activate', 'admin\LangueController@activate')->name('langue.activate');
Route::get('admin/langue/{id}/desactivate', 'admin\LangueController@desactive')->name('langue.desactive');