<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Animation</title>
  
<link rel="stylesheet" href="{{ asset('public/css/style_product.css')}}">
<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/css/style_slider.css')}}" media="all">
<link href="{{ asset('public/css/bootstrap.mins.css')}}" rel="stylesheet">


<style>
.section{
    flex: 1;
    flex-basis: 300px;+
    flex-grow: 0;
    height: 340px;
    background: #fff;
    border: 0px solid #fff;
    box-shadow: 0px 4px 7px rgba(0,0,0,.5);
    cursor: pointer;
    transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    overflow: hidden;
    position: relative;
	text-align:center;
}
.section img{
    width: 100%;
    height:65%;
    transition: all .5s cubic-bezier(.8,.5,.2,1.4);
}
.descriptions{
    position: absolute;
    top:0px;
    left:0px;
    background-color: rgba(255,255,255,.7);
    width:100%;
    height:100%;
    transition: all .7s ease-in-out;
    padding: 0px;
    box-sizing: border-box;
    clip-path: circle(0% at 100% 100%);
	text-align:right;
	
}
.section:hover .descriptions{
    left:0px;
    transition: all .7s ease-in-out;
    clip-path: circle(200%);
	width:100%;

}
.section:hover{
    transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    box-shadow: 0px 2px 3px rgba(0,0,0,.3);
    transform: scale(1);
    z-index: 9999;
}
.section:hover img{
    transition: all .5s cubic-bezier(.8,.5,.2,1.4);
    transform: scale(2);
    filter: blur(4px);
}



.section h1{
    color: #000000;
    letter-spacing: 1px;
    margin: 0px;
}
.section p{
    line-height: 24px;
    height: 70%;
}

 
.section .model_btn_product{margin-top:5px;transition: 1s;text-decoration:none;background-color:#ffffff;border:1px solid #856f2c;color:#856f2c;padding:5px;
width:50px;-webkit-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
-moz-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);}

.section .model_btn_product:hover{transition: 1s;text-decoration:none;background-color:#856f2c;border:1px solid #856f2c;color:#ffffff;padding:5px 15px 5px 5px;
width:100px;-webkit-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
-moz-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);}

.section .model_btn_product .model_btn_product_text{display:none;font-size:1px;color:#856f2c;}

.section .model_btn_product:hover .model_btn_product_text{display:none;font-size:14px;color:#ffffff;}

.modal{z-index:99999999;}
.img_full{width:100%;height:350px;}
</style>

<style>
.padding0{padding:0px;}
.space{width:100%;height:8px;}
.title1{font-size:30px;font-weight:blod;font-weight:600;padding:0px;text-align:center;}
.title1_b{color:#ffffff;font-size:30px;font-weight:blod;font-weight:600;padding:0px;text-align:center;}
.product_price{color:#000000;font-size:20px;font-weight:blod;font-weight:700;padding:10px;}

.product_title_model{font-size:26px;font-weight:blod;font-weight:700;padding:0px;}
.product_price_model{font-size:30px;font-weight:blod;font-weight:700;padding:0px;color:#856f2c;}
.product_decription_model{font-size:15px;font-weight:normal;font-weight:400;padding:0px;}

.product_title{font-size:16px;font-weight:normal;font-weight:400;padding:10px;}
.product_decription{font-size:15px;font-weight:normal;font-weight:400;padding:10px;}
.product_title_d{font-size:17px;font-weight:blod;font-weight:600;padding:10px;margin-top:120px;text-align:center;}
.product_decription_d{font-size:15px;font-weight:normal;font-weight:400;padding:10px;text-align:center;}
.textblod{color:#000000;font-size:15px;font-weight:blod;font-weight:700;}
.model_btn{background-color:#856f2c;border:1px solid#856f2c;color:#ffffff;padding:5px;width:100%;}

.qty_model{}
.qty_num_model{margin-left:80px;margin-top:-37px;width:100px;}
.modal-header{border-bottom:1px solid #ffffff;}
</style>






<!-- animation 2-->
<style>
.space_div{width:100%;height:10px;}
.space50{width:100%;height:50px;}

.textblodfull-title{font-size:34px;font-weight:normal;font-weight:400;text-align:center;}
.textblodfull{font-weight:blod;font-weight:700;}
.textnormalfull{font-size:15px;font-weight:normal;font-weight:400;}

.margin-res{margin: 0px 0px 15px 0px;}
 
.border-1w-full .model_btn{transition: 1s;text-decoration:none;background-color:#019d14;border:1px solid#019d14;color:#ffffff;padding:5px;width:100px;-webkit-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
-moz-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);}

.border-1w-full:hover .model_btn{transition: 1s;text-decoration:none;background-color:#019d14;border:1px solid#019d14;color:#ffffff;padding:5px 15px 5px 5px;width:150px;-webkit-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
-moz-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);}


.border-1w-full .incons-right{transition: 1s;margin-left:0px;}

.border-1w-full:hover .incons-right{transition: 1s;margin-left:30px;}

</style>
<style>

.border-1w-full{text-align:center;}
.border-1w-full:hover{text-align:center;background-color:#ffffff;transition: 2s;-webkit-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
-moz-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);}



.border-1w-full .border-1w{min-height:300px;text-align:center;background-image: url('{{ asset('public/image/dessin/3.jpg') }}');background-repeat: no-repeat;
background-size: 100% 100%;transition: 1s;}

.border-1w-full:hover .border-1w{min-height:300px;text-align:center;background-image: url('{{ asset('public/image/dessin/2.jpg') }}');background-repeat: no-repeat;
background-size: 100% 100%;transition: 1s;}

 .border-1w-full .border-1w-text{color:#000000;font-size:19px;transition: 1s;margin-top:0px;}
 .border-1w-full:hover .border-1w-text{color:#000000;font-size:24px;transition: 1s;margin-top:0px;}
 
 .border-1w-full .border-1w-text-show{transition: 1s;margin-top:200px;display: inline;}
 .border-1w-full:hover .border-1w-text-show{color:#000000;transition: 1s;margin-top:200px;display: inline;height:auto;}
</style>















<!-- animation 3-->
<style>
.textblodfull-title{font-size:34px;font-weight:normal;font-weight:400;text-align:center;}
.textblodfull{font-weight:blod;font-weight:700;}
.textnormalfull{font-size:15px;font-weight:normal;font-weight:400;}

.margin-res{margin: 0px 0px 15px 0px;}
 
.border-2w-full .model_btn{transition: 1s;text-decoration:none;background-color:#019d14;border:1px solid#019d14;color:#ffffff;padding:5px;width:100px;-webkit-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
-moz-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);}

.border-2w-full:hover .model_btn{transition: 1s;text-decoration:none;background-color:#019d14;border:1px solid#019d14;color:#ffffff;padding:5px 15px 5px 5px;width:150px;-webkit-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
-moz-box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);
box-shadow: 0px 6px 5px 0px rgba(138,134,138,1);}


.border-2w-full .incons-right{transition: 1s;margin-left:0px;}

.border-2w-full:hover .incons-right{transition: 1s;margin-left:30px;}

</style>
<style>
.img_bg1{transition: 1s;width:60px;height:60px;}
.border-2w-full:hover .img_bg1{transition: 1s;width:100px;height:100px;}
</style>

<style>
@media only screen and (min-width: 100px) {
  .bg1-text {background-color:#c0c0c0;height:auto;border:1px solid#f1f1f1;text-align:center;}
  .bg1-text-top{margin-top:30px;}
  .bg1{min-height:200px;text-align:center;background-image: url('{{ asset('public/image/dessin/8.gif') }}');background-repeat: no-repeat;background-size: 130% 100%;}
}
@media only screen and (min-width: 320px) {
  .bg1-text {background-color:#ffffff;height:auto;border:1px solid#f1f1f1;text-align:center;}
  .bg1-text-top{margin-top:30px;}
  .bg1{min-height:200px;text-align:center;background-image: url('{{ asset('public/image/dessin/8.gif') }}');background-repeat: no-repeat;background-size: 130% 100%;}
}
@media only screen and (min-width: 480px) {
  .bg1-text {background-color:#ffffff;height:auto;border:1px solid#f1f1f1;text-align:center;}
  .bg1-text-top{margin-top:30px;}
  .bg1{min-height:200px;text-align:center;background-image: url('{{ asset('public/image/dessin/8.gif') }}');background-repeat: no-repeat;background-size: 130% 100%;}
}



@media only screen and (min-width: 768px) {
  .bg1-text {background-color:#ffffff;height:auto;border:1px solid#f1f1f1;text-align:center;}
  .bg1-text-top{margin-top:70px;}
  .bg1{min-height:500px;text-align:center;background-image: url('{{ asset('public/image/dessin/8.gif') }}');background-repeat: no-repeat;background-size: 130% 100%;}
}

@media only screen and (min-width: 7068px) {
  .bg1-text {background-color:#ffffff;height:auto;border:1px solid#f1f1f1;text-align:center;}
  .bg1-text-top{margin-top:70px;}
  .bg1{min-height:500px;text-align:center;background-image: url('{{ asset('public/image/dessin/8.gif') }}');background-repeat: no-repeat;background-size: 130% 100%;}
}
</style>
</head>
<body>
@section('content')
@show



<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
	    <script>
      function myFunction(imgs) {
        var expandImg = document.getElementById("expandedImg");
        var imgText = document.getElementById("imgtext");
        expandImg.src = imgs.src;
        imgText.innerHTML = imgs.alt;
        expandImg.parentElement.style.display = "block";
      }
      </script>

</body>
</html>