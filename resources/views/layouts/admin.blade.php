<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('title')</title>
<meta name="description" content="Location app">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<link rel="stylesheet" href="{{ asset('public/css/model.css')}}" type="text/css" rel="stylesheet" media="screen">
<link href="{{ asset('public/themes/main.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/css/boos4.min.css')}}" type="text/css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="{{ asset('public/css/boos4-1.min.css')}}" type="text/css" rel="stylesheet" media="screen">

<link href="{{ asset('public/css/store_tables.min.css')}}" type="text/css" rel="stylesheet" media="screen">
<link rel="stylesheet" id="main-stylesheet" href="{{ asset('public/master_assets/styles/shards-dashboards.1.1.0.min.css')}}">
<link rel="stylesheet" href="{{ asset('public/master_assets/styles/extras.1.1.0.min.css')}}"> 
<link href="{{ asset('public/css/store.css')}}" type="text/css" rel="stylesheet" media="screen">
<link href="{{ asset('public/css/editpic.css')}}" type="text/css" rel="stylesheet" media="screen">
<link href="{{ asset('public/css/blog.css')}}" type="text/css" rel="stylesheet" media="screen">
<link href="{{ asset('public/css/search.css')}}" type="text/css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="{{ asset('public/css/drap.css')}}" type="text/css" rel="stylesheet" media="screen">


<script type="text/javascript" src="{{ asset('public/js/buttons.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/all.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/strap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/Chart.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/shards.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/jquery.sharrre.min.js')}}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>

</head>
<body>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
@include('layouts.admin.adminnavbarre')       
@include('layouts.admin.layout-options')
<div class="app-main pr">
@include('layouts.admin.adminsidebar')       
<div class="app-main__outer">
<div  class="app-main__inner app-body body-shadow {{ Auth::user()->body }}">
<div class="body_top">
@section('content')
@show
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="{{ asset('public/js/table/jquery.table.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/table/tables.view.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/input/search.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/back.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/blog.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/js/editpic.js')}}"></script>

<script type="text/javascript" src="{{ asset('public/themes/assets/scripts/main.js')}}"></script>

<script type="text/javascript" src="{{ asset('public/js/script.js')}}"></script>
<script>
$(document).ready(function() {
    // show the alert
    setTimeout(function() {
        $(".alert").alert('close');
    }, 2000);
});
</script>

<script>
$(document).ready(function() {
    $('#tableapp').DataTable();
} );
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
@yield('script')
</body>
</html>
