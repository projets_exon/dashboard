<div  class="app-sidebar sidebar-shadow  {{ Auth::user()->sidebar }}">
                    <div class="app-header__logo">
                        <div class="logo-srcss"></div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>

                    </div>

                    <div  class="scrollbar-sidebar {{ Auth::user()->sidebar_position }}">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li>
<a @if($page == 'Home')
class="nav-link active-link"
@else
class="nav-link"
@endif href="{{route('home', app()->getLocale())}}">
<i class="metismenu-icon pe-7s-home"></i>
{{ __('Accueil') }}
</a>
</li>

<li>
<a @if($page == 'Circuit')
class="nav-link active-link"
@else
class="nav-link"
@endif href="{{route('circuit.index', app()->getLocale())}}">
<i class="metismenu-icon pe-7s-map-2">
</i>{{ __('Circuit') }} 
</a>
</li>


<li>
<a @if($page == 'Point')
class="nav-link active-link"
@else
class="nav-link"
@endif href="{{route('point.index', app()->getLocale())}}">
<i class="metismenu-icon pe-7s-map-marker">
</i>{{ __('Point') }}
</a>
</li>

<li>
<a @if($page == 'Quiz')
class="nav-link active-link"
@else
class="nav-link"
@endif href="{{route('quiz.index', app()->getLocale())}}">
<i class="metismenu-icon pe-7s-help1">
</i>{{ __('Quiz') }}
</a>
</li>

<li>
<a @if($page == 'User')
class="nav-link active-link"
@else
class="nav-link"
@endif href="{{route('user.index', app()->getLocale())}}">
<i class="metismenu-icon pe-7s-users">
</i>{{ __('Utilisateurs') }}
</a>
</li>

<li>
<a @if($page == 'Admin')
class="nav-link active-link"
@else
class="nav-link"
@endif href="{{route('admin.index', app()->getLocale())}}">
<i class="metismenu-icon pe-7s-portfolio"></i>
{{ __('Administrateurs') }}
</a>
</li>


<li>
<a @if($page == 'Langue')
class="nav-link active-link"
@else
class="nav-link"
@endif href="{{route('langue.index', app()->getLocale())}}">
<i class="metismenu-icon pe-7s-flag">
</i>{{ __('Langue') }}
</a>
</li>




                            </ul>
                        </div>
                    </div>
                </div>
