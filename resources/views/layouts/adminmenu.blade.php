<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0 bgr-black">
          <div class="main-navbar">
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap border-bottom p-0 bgr-black" style="background-color:#ffffff;text-align:center;">
              <a class="navbar-brand w-100 mr-0" href="{{route('home')}}" style="line-height: 25px;">
                <div class="d-table m-auto" style="text-align:center;height:70px;">
                  <img style="width:140px;height:40px;" id="main-logo" class="d-inline-block align-top mr-1"  src="{{ asset('public/photo/logo/my-shop-logo-1583225903.jpg')}}" alt="Logo Dashboard">
                </div>
              </a>
              <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
              </a>
            </nav>
          </div>

          <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
            <div class="input-group input-group-seamless ml-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-search"></i>
                </div>
              </div>
              <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
          </form>
          <div class="nav-wrapper">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" id="menu-a" href="{{ route('home') }}">
                  <i class="fa fa-home"></i>
                  <span>Dashboard </span>
                </a>
              </li>


              <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('seo') }}">
                   <i class="fa fa-calendar"></i>
                   <span>SEO </span>
                </a>
              </li>

	            <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('cms.index') }}">
                   <i class="fa fa-calendar"></i>
                   <span>chasse aux fantômes CMS </span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('location') }}">
                   <i class="fa fa-map"></i>
                   <span>chasse aux fantômes location </span>
                </a>
              </li>



              <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('news.index') }}">
                  <i class="material-icons">view_module</i>
                  <span>Manage blog</span>
                </a>
              </li>

			  <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('contact.index') }}">
                  <i class="material-icons">view_module</i>
                  <span>Manage contact</span>
                </a>
              </li>

			  <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('manage-category.index') }}">
                   <i class="fa fa-calendar"></i>
                   <span>Manage category </span>
                </a>
              </li>

			  <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('product.index') }}">
                   <i class="fa fa-calendar"></i>
                   <span>Manage product </span>
                </a>
              </li>

			   <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('user.index') }}">
                  <i class="material-icons">view_module</i>
                  <span>Manage user</span>
                </a>
              </li>


			  <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('manage-admin.index') }}">
                  <i class="material-icons">view_module</i>
                  <span>Manage admin</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('booking.index') }}">
                  <i class="material-icons">view_module</i>
                  <span>Booking</span>
                </a>
              </li>

			   <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('stock.index') }}">
                  <i class="material-icons">view_module</i>
                  <span>In Stock</span>
                </a>
              </li>

			   <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('settings.index') }}">
                  <i class="material-icons">view_module</i>
                  <span>chasse aux fantômes settings</span>
                </a>
              </li>

			  <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('manage-statistics.index') }}">
                  <i class="material-icons">view_module</i>
                  <span>Statistics</span>
                </a>
              </li>

			   <li class="nav-item">
                <a class="nav-link " id="menu-a" href="{{ route('newsletter.index') }}">
                  <i class="material-icons">view_module</i>
                  <span>Subscribe newsletter</span>
                </a>
              </li>
			<li class="nav-item">
                <a class="nav-link " id="menu-a" href="">
                  <i class="fa fa-file"></i>
                  <span>Documents</span>
                </a>
              </li>



              <!--<li class="nav-item">
                <a class="nav-link " href="">
                  <i class="fa fa-users"></i>
                  <span>Users</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="">
                  <i class="material-icons">view_module</i>
                  <span>Records</span>
                </a>
              </li>
			  <li class="nav-item">
                <a class="nav-link " href="<?php echo date('Y');?>">
                  <i class="material-icons">table_chart</i>
                  <span>Statistic </span>
                </a>
              </li>

<li class="nav-item">
                <a class="nav-link " href="user-profile-lite.html">
                  <i class="material-icons">person</i>
                  <span>User Profile</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="errors.html">
                  <i class="material-icons">error</i>
                  <span>Errors</span>
                </a>
              </li>-->
            </ul>
          </div>
        </aside>
