@extends('layouts.admin')
@section('title', 'Dashbord')
@section('content')
<?php $page = "Home";?>
<div class="container">
<div class="row">
<div class="py-4 col-lg-12 col-md-12 col-sm-12 mb-4 row">
<div class="row" style="margin-left:0px;">
<div class="col-md-8 col-sm-12 mb-12">
<h3 class="page-title"style="" >{{ __('Votre') }}  {{ __('Accueil') }}</h3>
</div>


<div class="col-md-4 col-sm-12 mb-12 padding0 text-right">
<button onclick="window.location.href='{{route('langue.index', app()->getLocale())}}'" class="btn_dachboard"><i class="fa fa-cog floatleftincons"></i>{{ __('Paramètres') }} </button>

<div class="btn-group">
<button data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn_dachboard_auto2">
@if(Auth::user()->dashboard=='1')
Version 1
@elseif(Auth::user()->dashboard=='2')
Version 2
@elseif(Auth::user()->dashboard=='3')
Version 3
@endif
</button>
<div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
<form class="p-0 btn"  method = "POST">
@csrf
<div class="btn-group">
<input type="hidden" name="_method" value="PATCH">
<input type="hidden" name="dashboard" value="1"/>
<button type="submit" class="dropdown  p-0 btn">
<a type="button" tabindex="0" class="dropdown-item">
Version 1
</a>
</button>
</div>
</form>
<form class="p-0 btn" method = "POST">
@csrf
<div class="btn-group">
<input type="hidden" name="_method" value="PATCH">
<input type="hidden" name="dashboard" value="2"/>
<button type="submit" class="dropdown  p-0 btn">
<a type="button" tabindex="0" class="dropdown-item">
Version 2
</a>
</button>
</div>
</form>
</div>
</div>
</div>
</div>


<div class="row" style="padding:10px;margin-left:0px;">
<div class="stats-small stats-small--1 card card-small" >
<div class="card-body p-0 d-flex">
<div class="col-md-6 col-sm-12 mb-12">
<div class="stats-small__data text-center" style="margin-top:50px;">
<h6 class="stats-small__value count my-3">{{ __('Bonjour') }} {{ Auth::user()->firstname }}</h6>
<span class="stats-small__label text-uppercase textnormal">{{ Auth::user()->email }}</span>

</h3>
</div>
</div>
<div class="col-md-6 col-sm-12 mb-12">
<img src="{{ asset('public/image/undraw/all_the_data.svg')}}" style="width:200px;height:200px;">
</div>
</div>
</div>
</div>

<div class="page-header no-gutters col-md-8 col-sm-12 mb-12">
@if(session()->get('success'))
<div class="modal fade" id="overlay">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-body" style="text-align:center;">
<label class="stats-small__value count my-3 textblodfull"> {{ __('Bienvenue') }}</label>
<img style="width:140px;height:25px;" id="main-logo" class="d-inline-block align-top mr-1"  src="{{ asset('public/photo/logo/logo.png')}}" alt="Logo Dashboard" >
<p class="textnormalfull" style="font-size:20px;color:#000000;">
{{ session()->get('success') }}</p>
</div>
</div>
</div>
</div>
@endif
</div>
</div>






<div class="row">

<div class="col-md-3 col-sm-12 mb-12" onclick="window.location.href='{{route('circuit.index', app()->getLocale())}}'" style="cursor: pointer;">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left1">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/user_flow.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3"></h6>
<span class="stats-small__label text-uppercase textnormal">{{ __('Circuit') }}</span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>


<div class="col-md-3 col-sm-12 mb-12" onclick="window.location.href='{{route('user.index', app()->getLocale())}}'" style="cursor: pointer;">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/web_devices.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3"></h6>
<span class="stats-small__label text-uppercase textnormal">{{ __('Utilisateurs') }}</span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>

<div class="col-md-3 col-sm-12 mb-12" onclick="window.location.href='{{route('point.index', app()->getLocale())}}'" style="cursor: pointer;">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left1">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/work_chat.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3"></h6>
<span class="stats-small__label text-uppercase textnormal">{{ __('Point') }}</span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>



<div class="col-md-3 col-sm-12 mb-12" onclick="window.location.href='{{route('admin.index', app()->getLocale())}}'" style="cursor: pointer;">
<div class="stats-small stats-small--1 card card-small">
<div class="card-body p-0 d-flex" id="border-left">
<div class="d-flex flex-column m-auto">
<div class="stats-small__data text-center">
<div class="col-lg-12 col-md-12 col-sm-12"><img src="{{ asset('public/image/undraw/world.svg') }}" style="width:100px;height:100px;margin-top:10px;"></div>
<h6 class="stats-small__value count my-3"></h6>
<span class="stats-small__label text-uppercase textnormal">{{ __('Administrateurs') }}</span>
<div class="space"></div>
</div>
</div>
</div>
</div>
</div>
</div>





<div class="space"></div>
<div class="space"></div>



<div class="row">
<div class="col-lg-8 col-sm-12 mb-12">
<div class="card card-small h-100">
<div class="card-header border-bottom">
<h6 class="m-0">{{ __('Nos joueurs') }}</h6>
</div>
<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
</div>
<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
<div style="position:absolute;width:200%;height:100%;left:0; top:0"></div>
</div>
</div>
<canvas id="chart-line" class="chartjs-render-monitor" style="display: block; width: 299px; height:100px;"></canvas>
</div>
</div>


<div class="col-lg-4 col-sm-12 mb-12">
<div class="card card-small h-100">
<div class="card-header border-bottom">
<h6 class="m-0">{{ __('nombre de points dans un circuit') }} </h6>
</div>
<div class="card-body d-flex py-0">
<canvas height="220"id="pieChart"></canvas>
 </div>
<div class="card-footer border-top">
<div class="row">

</div>
</div>
</div>
</div>
</div>

</div>
</div>

</div>
</div>
</div>

            
<script>
var ctxP = document.getElementById("pieChart").getContext('2d');
var myPieChart = new Chart(ctxP, {
type: 'pie',
data: {
labels: ["User ( 4 )", "Admin ( 1 )"],
datasets: [{
data: [3, 5],
backgroundColor: ["#052ab4", "#6a84e8"],
hoverBackgroundColor: ["#FF5A5E", "#5AD3D1"]
}]
},
options: {
responsive: true
}
});
</script>

<script>
    $(document).ready(function() {
        var ctx = $("#chart-line");
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
                datasets: [{
                   
                    data: [2, 3, 3, 4, 7, 7, 4, 4, 7, 5, 5, 4],
                    label: "Male",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [2, 3, 3, 4, 7, 7, 4, 4, 7, 5, 5, 4],
                    label: "Female",
                    borderColor: "#3cba9f",
                    fill: false
                }]
            },
            options: {title: {
                    display: true,
                    text: 'Clients year {{date("Y")}}'
                }
            }
        });
    });
</script>
@endsection
