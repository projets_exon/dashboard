@extends('layouts.admin')
@section('title', 'Login History')
@section('content')

    <div class="container">
        <div class="row titleapp">
            <div class="col-md-7">
                <span class="mt-4 pagetitle">Login History</span>
            </div>
            <div class="col-md-5" style="text-align:right;">
                <a id="pagetitleprimerlink" href="{{ route('home') }}">Dashbord /</a>
                <a id="pagetitlesecondelink">Profile </a>
            </div>
        </div>
        </br>

        <div class="row">
            <div class="col-lg-12  px-3">
                <div class="card card-small mb-4 px-3">
                    <div class="space"></div>
                    <div class="space"></div>
                    <div> <h1>Login History</h1></div>
                    <div class="space"></div>
                    <div class="space"></div>
                    <table id="dtBasicExample" class="table  table-sm" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th scope="col">Login Time</th>
                            <th scope="col">Source  IP</th>
                            <th scope="col">Country</th>
                            <th scope="col">City</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($historys as $history)
                            <tr>
                              
                                <td>{{$history->last_login_at}}</td>
                                <td>{{$history->last_login_ip}}</td>
                                <td>{{$history->country}}</td>
                                <td>{{$history->city}}</td>
                              </tr>
                            @endforeach
                         
                        </tbody>
                    </table>      
                    <div class="space"></div>
                    <div class="space"></div>
                </div>
            </div>
        </div>
    </div>

   <script>
    // Basic example
$(document).ready(function () {
  $('#dtBasicExample').DataTable({
    "pagingType": "simple" // "simple" option for 'Previous' and 'Next' buttons only
  });
  $('.dataTables_length').addClass('bs-select');
});
</script>
@endsection
