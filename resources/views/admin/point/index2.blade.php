@extends('layouts.admin')
@section('title', 'Manage Point')
@section('content')

<style>
.hide {
  display: none;
}
</style>
<div class="container"></div>
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >{{ __(' Gérer le point') }}</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home', app()->getLocale()) }}">{{ __('Accueil') }} /</a>
<a id="pagetitlesecondelink">{{ __('Point') }} </a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >{{ __('Point') }} ( {{ $Point->count() }} )</span>
</div>
<div class="col-md-5 text-right">
<a class="btn-switch-active" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

@if(session()->get('success'))
<div class="alert {{ session()->get('success_alert') }} alert-dismissible fade show" role="alert">
<strong>{{ session()->get('success') }}</strong>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
@endif

<div class="row details">
<div class="col-12">
<table id="tableapp" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr class="text-center">
<th>{{ __('Titre') }}</th>
<th>{{ __('Longitude') }}</th>
<th>{{ __('Lattitude') }}</th>
<th>{{ __('Date') }}</th>
<th>{{ __('Type') }}</th>
<th >{{ __('État') }}</th>
<th>{{ __('Action') }}</th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Point as $post)
<div class="modal fade" id="delete{{ $post->idPoint }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-body" style="text-align:center;">
<h5 class="textbloddelete">{{ __('Supprimer') }} ?</h5>
<h5 class="textnormaldelete ">{{ __('Veuillez vous assurer et ensuite confirmer') }}!</h5>
</div>
<div class="modal-footer">
<form action = "{{ route('point.destroy', $post->idPoint) }}" method = "POST">
@csrf
@method('PATCH')
<a type="button" class="model_btn_close" data-dismiss="modal">{{ __('NON') }}, {{ __('annuler') }}</a>
<button type="submit" class="model_btn_delete delete-post">{{ __('OUI') }}, {{ __('Supprimer') }}{{$post->idPoint}}</button>
</form>
</div>
</div>
</div>
</div>

<div class="modal fade" id="edit{{ $post->idPoint }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title textblod">{{ __('Modifier') }}</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<form action = "{{ route('point.update', $post->idPoint) }}" method = "POST">
<div class="modal-body">
@csrf
@method('PATCH')
<div class="form-group">
<label for="recipient-name" class="textblod">{{ __('Titre') }}</label>
<input type="text" class="form-control" name="title" value="{{$post->title}}" />
</div>

<div class="form-group">
<label for="recipient-name" class="textblod">{{ __('Longitude') }}</label>
<input type="text" class="form-control" name="longitude" value="{{$post->longitude}}" />
</div>

<div class="form-group">
<label for="recipient-name" class="textblod">{{ __('Lattitude') }}</label>
<input type="text" class="form-control" name="lattitude" value="{{$post->lattitude}}" />
</div>

<div class="form-group">
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox={{$post->longitude}}%2C{{$post->lattitude}}%2C{{$post->longitude}}%2C{{$post->lattitude}}&amp;layer=mapnik&amp;marker={{$post->lattitude}}%2C{{$post->longitude}}#map=5" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat={{$post->lattitude}}&amp;mlon={{$post->longitude}}#map=11/{{$post->lattitude}}/{{$post->longitude}}">Afficher une carte plus grande</a></small>
</div>



<div class="form-group">
<label for="post-description" class="textblod">A{{ __('Activation') }}</label>
<label for="r2" class="textnormal">
<input type="radio" name="active" id="r2"onClick="getResults()" value="1"
@if($post->active == 1)
checked
@else
@endif > {{ __('Active') }}
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="active" id="r1" onClick="getResults()" value="0"
@if($post->active == 0)
checked
@else
@endif> {{ __('Bloqué') }}
</label>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
<button type="submit" class="model_btn_save">{{ __('Sauvegarder') }}</button>
</div>
</form>

</div>
</div>
</div>


<div class="modal fade" id="point_type{{ $post->idPoint }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title textblod">{{ __('Modifier') }}</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>

<form action = "{{ route('point.type', $post->idPoint) }}" method = "POST" enctype="multipart/form-data">
@csrf
@method('PATCH')
<div class="modal-body">
<div class="form-group">
<label for="home" class="control-label textblod">Type</label>

<div class="">
<label  class="textnormal">
Image <input type="radio" name="type" value="1" onclick="imagevideo{{ $post->idPoint }}();" />
</label>

<label  class="textnormal margin20">
Video <input type="radio" name="type" value="2" onclick="imagevideo{{ $post->idPoint }}();" />
</label>

<label class="textnormal margin10">
Url video <input type="radio" name="type" value="3" onclick="urlvideo{{ $post->idPoint }}();" />
</label>

</div>
</div>

<div id="inputfile{{ $post->idPoint }}" class="hide">
<div class="form-group">
<label for="home" class="control-label textblod">Upload file</label>
<div class="">
<input type="file" name="image"  />
</div>
</div>
</div>

<div id="inputurl{{ $post->idPoint }}" class="hide">
<div class="form-group">
<label for="home" class="control-label textblod">Url video</label>
<div class="">
<input type="text"  class="form-control input-lg" name="image"  />
</div>
</div>
</div>
</div>
<div class="modal-footer">
<a type="button" class="model_btn_close" data-dismiss="modal">{{ __('NON') }}, {{ __('annuler') }}</a>
<button type="submit" class="model_btn_save delete-post">{{ __('OUI') }}, {{ __('Modifier') }}</button>
</div>
</form>
</div>
</div>
</div>






<div class="modal fade" id="point_type_edit{{ $post->idPoint }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title textblod">{{ __('Modifier') }}</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>

<form action = "{{ route('point.type', $post->idPoint) }}" method = "POST" enctype="multipart/form-data">
@csrf
@method('PATCH')
<div class="modal-body">
<div class="form-group">
<label for="home" class="control-label textblod">Type</label>
<div class="">
<div class="">
<label  class="textnormal">
Image <input type="radio" name="type" value="1" onclick="imagevideo{{ $post->idPoint }}();" @if($post->type == '1')
checked
@else
@endif />
</label>

<label  class="textnormal margin20">
Video <input type="radio" name="type" value="2" onclick="imagevideo{{ $post->idPoint }}();" @if($post->type == '2')
checked
@else
@endif />
</label>

<label class="textnormal margin10">
Url video <input type="radio" name="type" value="3" onclick="urlvideo{{ $post->idPoint }}();" @if($post->type == '3')
checked
@else
@endif />
</label>
</div>
</div>
</div>


<div id="inputfile_edit{{ $post->idPoint }}" class="hide">
<div class="form-group">
<label for="home" class="control-label textblod">Upload file</label>
<div class="">
<input type="file" name="image"  />
</div>
</div>
</div>

<div id="inputurl_edit{{ $post->idPoint }}" class="hide">
<div class="form-group">
<label for="home" class="control-label textblod">Url video</label>
<div class="">
<input type="text"  class="form-control input-lg" name="image"  />
</div>
</div>
</div>


@if($post->type == '1')
<img class="user-avatar mr-2" src="{{ asset('public/file_point/image/'.$post->image) }}" style="width:100%;height:auto;min-height:300px;" alt="Logo">
@elseif($post->type == '2')
<video src="{{ asset('public/file_point/video/'.$post->image) }}" style="width:100%;height:auto;min-height:300px;" controls>
</video>
@elseif($post->type == '3')
<iframe src="{{ $post->image }}" style="width:100%;height:auto;min-height:300px;">
</iframe>
@endif


<div class="form-group">
<label for="home" class="control-label textblod">Upload file</label>
<div class="">
<input type="file" name="image"  />
</div>
</div>
</div>

<div class="modal-footer">
<a type="button" class="model_btn_close" data-dismiss="modal">{{ __('NON') }}, {{ __('annuler') }}</a>
<button type="submit" class="model_btn_save delete-post">{{ __('OUI') }}, {{ __('Modifier') }}</button>
</div>
</form>
</div>
</div>
</div>


<tr id="post_id_{{ $post->idPoint }}">
<td>
<a class="table_link" href="">
{{ $post->title  }}
</a>
</td>
<td>
<a class="table_link" href="">
{{ $post->longitude  }}
</a>
</td>
<td>
<a class="table_link" href="">
{{ $post->lattitude  }}
</a>
</td>

<td>
{{ date('d/m/Y', strtotime($post->created_at)) }}
</td>
<td>
<a class="table_link" href="">
@if($post->type == Null)
<a href="javascript:void(0)" data-toggle="modal" data-target="#point_type{{ $post->idPoint }}"><span class="text-danger">N / A</span></a>
@else
@if($post->type == '1')
<a href="javascript:void(0)" data-toggle="modal" data-target="#point_type_edit{{ $post->idPoint }}" class="text-primary">Image</a>
@elseif($post->type == '2')
<a href="javascript:void(0)" data-toggle="modal" data-target="#point_type_edit{{ $post->idPoint }}"><span class="text-drak">Video</span></a>
@elseif($post->type == '3')
<a href="javascript:void(0)" data-toggle="modal" data-target="#point_type_edit{{ $post->idPoint }}"><span class="text-drak">Url video</span></a>
@endif
@endif
</a>
</td>
<td class="text-center">
@if($post->active =='1')
<a href="{{ route('point.destroy', $post->idPoint) }}"><i class="fa fa-check active_color" aria-hidden="true"></i></a>
@else
<a href="{{ route('point.activate', $post->idPoint) }}" ><i class="fa fa-lock desactive_color" aria-hidden="true"></i></a>
@endif
</td>

<td class="td_btn">
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit{{ $post->idPoint }}" class="btn-delete"><i class="fa fa-edit"></i></a>
<a href="javascript:void(0)" data-toggle="modal" data-target="#delete{{ $post->idPoint }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
</td>
</tr>
<script>
function imagevideo{{ $post->idPoint }}(){
document.getElementById('inputfile{{ $post->idPoint }}').style.display = 'block';
document.getElementById('inputurl{{ $post->idPoint }}').style.display = 'none';
document.getElementById('inputfile_edit{{ $post->idPoint }}').style.display = 'block';
document.getElementById('inputurl_edit{{ $post->idPoint }}').style.display = 'none';
}
function urlvideo{{ $post->idPoint }}(){
document.getElementById('inputfile{{ $post->idPoint }}').style.display = 'none';
document.getElementById('inputurl{{ $post->idPoint }}').style.display = 'block';
document.getElementById('inputfile_edit{{ $post->idPoint }}').style.display = 'none';
document.getElementById('inputurl_edit{{ $post->idPoint }}').style.display = 'block';
}
</script>
@endforeach
</tbody>
</table>
</div>
</div>
@endsection

