@extends('layouts.admin')
@section('title', 'Manage category')
@section('content')
<?php $page = "Point";?>
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >{{ __('Gérer la catégorie') }} </span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home', app()->getLocale()) }}">{{ __('Accueil') }} /</a>
<a id="pagetitleprimerlink" href="{{ route('circuit.index', app()->getLocale()) }}">{{ __('catégorie') }}  /</a>
<a id="pagetitlesecondelink">{{ __('Ajouter') }}</a>
</div>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >{{ __('catégorie') }} </span>
</div>
<div class="col-md-5 text-right">
@if(Auth::user()->role=='3')
@else
<a class="btn-switch-active"><i class="fa fa-plus" aria-hidden="true"></i></a>
@endif
<a href="{{ route('circuit.index', app()->getLocale()) }}" class="btn-switch" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a>
</div>
</div>

<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">{{ __('Ajouter') }} {{ __('Nouveau') }}{{ __('catégorie') }}</h3>
</div>



<div class="row">
<div class="col-lg-12 cadre_filter">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif

<form id="file-upload-form" class="uploader" action="{{ route('circuit.store', app()->getLocale()) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<input type="hidden" name="active" id="active" value="1">
<input type="hidden" name="parent_id" id="parent_id" value="1">
<input type="hidden" name="lang_defaut" id="lang_defaut" value="{{ $lang_id }}">

<div class="row">
<div class="form-group col-md-6">
<label for="name" class="col-sm-12 control-label textblod">Title</label>
<div class="col-sm-12">
<input type="text" id="title" name="title" class="form-control input-lg" required="">
<span id="error_email"></span>
</div>
</div>




<div class="form-group col-md-6">
<label for="home" class="col-sm-12 control-label textblod">Active</label>
<div class="col-sm-12">
<label for="1" class="textnormal">YES
<input type="radio" id="1" name="active"  value="1" checked>
</label>
<label for="2" class="textnormal margin20">NO
<input type="radio" id="2" name="active"  value="0" >
</label>
</div>
</div>
</div>


<div class="modal-footer"style="text-align:right;">
<button type="submit" name="register" id="register"  class="model_btn_save">Save</button>
</div>
</form>
</div>
</div>
</div>
<script>
$('div.form-group-max').maxlength();
</script>
@endsection
