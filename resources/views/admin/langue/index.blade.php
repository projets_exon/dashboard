@extends('layouts.admin')
@section('title', 'Manage language')
@section('content')
<?php $page = "Langue";?>
<div class="container"></div>
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage Language</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home', app()->getLocale()) }}">Dashbord /</a>
<a id="pagetitlesecondelink">Language</a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Language ( {{ $Language->count() }} )</span>
</div>
<div class="col-md-5 text-right">
<a class="btn-switch-active" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

@if(session()->get('success'))
<div class="alert {{ session()->get('success_alert') }} alert-dismissible fade show" role="alert">
<strong>{{ session()->get('success') }}</strong>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
@endif

<div class="row details">
<div class="col-12">
<table id="tableapp" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr class="text-center">
<th>Name</th>
<th>symbole</th>
<th>Date</th>
<th >Etat</th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Language as $post)
<tr id="post_id_{{ $post->idLanguage }}">
<td>
<a class="table_link" href="">
<img class="user-avatar rounded-circle mr-2" src="{{ asset('public/photo/language_logo/'.$post->photo) }}" style="width:25px;height:25px;" alt="Logo">
{{ $post->labelLang  }}
</a>
</td>
<td>
{{ $post->symbole  }}

</td>
<td>
{{ date('d/m/Y', strtotime($post->created_at)) }}
</td>
<td class="text-center">
@if($post->active =='1')
<a href="{{ route('langue.desactive', $post->idLanguage) }}"><i class="fa fa-check active_color" aria-hidden="true"></i></a>
@elseif($post->active =='0')
<a href="{{ route('langue.activate', $post->idLanguage) }}" ><i class="fa fa-lock desactive_color" aria-hidden="true"></i></a>
@endif
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
@endsection

