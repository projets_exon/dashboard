@extends('layouts.admin')
@section('title', 'Manage Admin')
@section('content')
<?php $page = "Admin";?>
<div class="container"></div>
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage Admin</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home', app()->getLocale()) }}">Dashbord /</a>
<a id="pagetitlesecondelink">Admin</a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Admin ( {{ $User->count() }} )</span>
</div>
<div class="col-md-5 text-right">
<a class="btn-switch-active" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

@if(session()->get('success'))
<div class="alert {{ session()->get('success_alert') }} alert-dismissible fade show" role="alert">
<strong>{{ session()->get('success') }}</strong>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
@endif

<div class="row details">
<div class="col-12">
<table id="tableapp" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr class="text-center">
<th>FirstName</th>
<th>LastName</th>
<th>Date</th>
<th >Etat</th>
<th>Action</th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($User as $post)
<div class="modal fade" id="delete{{ $post->id }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-body" style="text-align:center;">
<h5 class="textbloddelete">Delete ?</h5>
<h5 class="textnormaldelete ">Please ensure and then confirm!</h5>
</div>
<div class="modal-footer">
<form action = "{{ route('admin.destroy', $post->id) }}" method = "POST">@csrf
@method('PATCH')
<a type="button" class="model_btn_close" data-dismiss="modal">No, cancel</a>
<button type="submit" class="model_btn_delete delete-post">Yes, Delete {{$post->id}}</button>
</form>
</div>
</div>
</div>
</div>


<tr id="post_id_{{ $post->id }}">
<td>
<a class="table_link" href="">
{{ $post->firstName  }}
</a>
</td>
<td>
{{ $post->lastName  }}

</td>
<td>
{{ date('d/m/Y', strtotime($post->created_at)) }}
</td>
<td class="text-center">
@if($post->active =='1')
<a href="{{ route('admin.desactive', $post->id) }}"><i class="fa fa-check active_color" aria-hidden="true"></i></a>
@elseif($post->active =='0')
<a href="{{ route('admin.activate', $post->id) }}" ><i class="fa fa-lock desactive_color" aria-hidden="true"></i></a>
@endif
</td>

<td class="td_btn">
<a href="javascript:void(0)" data-toggle="modal" data-target="#delete{{ $post->id }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
@endsection

