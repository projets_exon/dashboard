@extends('layouts.admin')
@section('title', 'Manage Circuit')
@section('content')
<?php $page = "Circuit";?>
<div class="container"></div>
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >{{ __('Gérer le circuit') }}</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home', app()->getLocale()) }}">{{ __('Accueil') }}/</a>
<a id="pagetitlesecondelink">{{ __('Circuit') }}</a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Add new circuit</span>
</div>
<div class="col-md-5 text-right">
<a href="{{ route('circuit.create', app()->getLocale()) }}" class="btn-switch-active" title="Add new"><i class="fa fa-plus" aria-hidden="true"></i></a>
<a href="{{ route('circuit.index', app()->getLocale()) }}" class="btn-switch" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row">
<div class="col-lg-12 cadre_filter">
@if(Session::has('success'))
<div class="alert alert-success">
{{Session::get('success')}}
</div>
@endif


<form action="{{ route('circuit.store', app()->getLocale()) }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<section>
		
<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
@csrf
<label class="label textblod">Title:</label>
<input type="text" name="title" class="form-control" value="{{ old('title') }}" />
<span class="text-danger">{{ $errors->first('title') }}</span>
</div>
<div class="panel panel-footer">

<div class="row" style="padding:10px 0px 5px 0px;">
<div class="col-md-12">
<span class="mt-4 pagetitle" >Add points</span>
</div>
</div>
                    <table class="table table-bordered" id="crud_table">
                        <thead>
                            <tr class="textblod">
                                <th>Title</th>
                                <th>Longitude</th>
                                <th>Lattitude</th>
                                <th>
                                    <a href="#" class="btn btn-primary addRow">
                                        <i class="material-icons">add</i>
                                    </a>
                                </th>
                            </tr>
                        </thead>
                                @if(old('row')!= '')
                                    <tbody class="validation-tbody-destination" id="validation-tbody-destination">
                                        <?php 
                                        $old_row = old('row');
                                        $rowReplace = count($old_row);
                                        ?>
                                        @foreach($old_row as $key=>$value)
                                            <tr class="num-row main_data validation" id="num-row">
                                                <td>
                                                    <div class="form-group {{ $errors->has("row.$key.title") ? 'has-error' : '' }}">
                                                        <input type="text" name="row[{{$key}}][title]" class="form-control title" id="title" value="{{ $value['title'] }}">
                                                        <span class="text-danger">{{ $errors->first("row.$key.title") }}</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group {{ $errors->has("row.$key.longitude") ? 'has-error' : '' }}">
                                                        <input type="text" name="row[{{$key}}][longitude]" class="form-control" value="{{ $value['longitude'] }}">
                                                        <span class="text-danger">{{ $errors->first("row.$key.longitude") }}</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group {{ $errors->has("row.$key.lattitude") ? 'has-error' : '' }}">
                                                        <input type="text" name="row[{{$key}}][lattitude]" class="form-control"  value="{{ $value['lattitude'] }}">
                                                        <span class="text-danger">{{ $errors->first("row.$key.lattitude") }}</span>
                                                    </div>
                                                </td>
                                                <td><a href="#" class="btn btn-danger remove"><i class="material-icons">remove</i></a></td>
                                            </tr>
											
                                        @endforeach 
                                    </tbody>
                                @else
                                    <tbody class="original-tbody-destination" id="original-tbody-destination">
                                    </tbody>
                                @endif
                        <tfoot>
                            <tr>
                                <td><input type="submit" value="Submit" class="btn btn-success submit" id="submit"></td>
                            </tr>
                        </tfoot>
                    </table>
            </section>
        </form>
        </div>
        </div>

@endsection
@section('script')
<script type="text/javascript">
    $('tbody .main_data').delegate('.title,.description,.longitude,.lattitude,.image','keyup',function(){
        var tr = $(this).parent().parent();
        var title=tr.find('.title').val();
        var longitude=tr.find('.longitude').val();
        var lattitude=tr.find('.lattitude').val();
    });
    var int = "{{ $rowReplace }}"; 
    function addRow(){
            var tr='<tr class="table_field main_data " id="table_field-'+ (int) +'">'+
                '<td><input type="text" name="row['+ (int) +'][title]" class="form-control" id="title"></td>'+
                '<td><input type="text" name="row['+ (int) +'][longitude]" class="form-control" id="longitude"></td>'+
                '<td><input type="text" name="row['+ (int) +'][lattitude]" class="form-control" id="lattitude"></td>'+
                '<td><a href="#" class="btn btn-danger remove"><i class="material-icons">remove</i></a></td>'+
                '</tr>';
            $('.original-tbody-destination').append(tr);
            int++;
    };
    $(function() {
        $('.addRow').trigger("click");
    });
    $('.addRow').on('click',function(){
        addRow();
    });
    $(document).on("keypress keyup blur",'.description',function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('.title').bind('keyup blur',function(){ 
        var node = $(this);
        node.val(node.val().replace(/[^a-zA-Z]/g,'') ); 
    });
    $('.title').bind('keyup blur',function(){ 
        var node = $(this);
        node.val(node.val().replace(/[^a-zA-Z ]/g,'') );
    });
    $(document).on("keypress keyup blur",'.longitude',function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(document).ready(function () {
        $(".original-tbody-destination").on('input', '.description', function () {
                var calculated_total_sum = 0;
                $(".original-tbody-destination .description").each(function () {
                    var get_textbox_value = $(this).val();
                        if($.isNumeric(get_textbox_value)) {
                            calculated_total_sum += parseFloat(get_textbox_value);
                        }                 
                });
        $("#total").html(calculated_total_sum);
        });
    });
    $(document).ready(function () {
        $(".validation-tbody-destination").on('input', '.description', function () {
                var calculated_total_sum = 0;
                $(".validation-tbody-destination .description").each(function () {
                    var get_textbox_value = $(this).val();
                        if($.isNumeric(get_textbox_value)) {
                            calculated_total_sum += parseFloat(get_textbox_value);
                        }                 
                });
        $("#total").html(calculated_total_sum);
        });
    });
    $(document).ready(function () {
        $(".original-tbody-destination").on('input','.longitude', function () {
            var calculated_total_sum = 0;
            $(".original-tbody-destination .longitude").each(function () {
                var get_textbox_value = $(this).val();
                if($.isNumeric(get_textbox_value)) {
                    calculated_total_sum += parseFloat(get_textbox_value);
                }                  
            });
        $("#obtain").html(calculated_total_sum);
        });
    });
    $(document).ready(function () {
        $(".validation-tbody-destination").on('input','.longitude', function () {
            var calculated_total_sum = 0;
            $(".validation-tbody-destination .longitude").each(function () {
                var get_textbox_value = $(this).val();
                if($.isNumeric(get_textbox_value)) {
                    calculated_total_sum += parseFloat(get_textbox_value);
                }                  
            });
        $("#obtain").html(calculated_total_sum);
        });
    });
    $(document).on('click', '.remove', function(){
        var delete_row = $(this).data(".table_field");
            $(this).parent().parent().remove();
            $(".description").trigger("input");
            $(".longitude").trigger("input");
        });
</script>
@endsection