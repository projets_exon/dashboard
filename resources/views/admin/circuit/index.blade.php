@extends('layouts.admin')
@section('title', 'Manage Circuit')
@section('content')
<?php $page = "Circuit";?>
<div class="container"></div>
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >{{ __('Gérer le circuit') }}</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home', app()->getLocale()) }}">{{ __('Accueil') }}/</a>
<a id="pagetitlesecondelink">{{ __('Circuit') }}</a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Circuit ( {{ $Circuit->count() }} )</span>
</div>
<div class="col-md-5 text-right">
@if(Auth::user()->role=='3')
@else
<a href="{{ route('circuit.create', app()->getLocale()) }}" class="btn-switch" title="Add new"><i class="fa fa-plus" aria-hidden="true"></i></a>
@endif
<a class="btn-switch-active" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

@if(session()->get('success'))
<div class="alert {{ session()->get('success_alert') }} alert-dismissible fade show" role="alert">
<strong>{{ session()->get('success') }}</strong>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
@endif

<div class="row details">
<div class="col-12">
<table id="tableapp" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr class="text-center">
<th>{{ __('Titre') }}</th>
<th>{{ __('Nombre de points') }}</th>
<th>{{ __('Date') }}</th>
<th >{{ __('État') }}</th>
<th>{{ __('Action') }}</th>
</tr>
</thead>
<tbody id="posts-crud">
@foreach($Circuit as $post)
<div class="modal fade" id="delete{{ $post->idCircuit }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-body" style="text-align:center;">
<h5 class="textbloddelete">{{ __('Supprimer') }}?</h5>
<h5 class="textnormaldelete "> {{ __('Veuillez vous assurer et ensuite confirmer') }}!</h5>
</div>
<div class="modal-footer">
<form action = "{{ route('circuit.destroy', $post->idCircuit) }}" method = "POST">
@csrf
@method('PATCH')
<a type="button" class="model_btn_close" data-dismiss="modal">{{ __('NON') }}, {{ __('annuler') }}</a>
<button type="submit" class="model_btn_delete delete-post">{{ __('OUI') }}, {{ __('Supprimer') }} {{$post->idCircuit}}</button>
</form>
</div>
</div>
</div>
</div>

<div class="modal fade" id="edit{{ $post->idCircuit }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title textblod">{{ __('Modifier') }}</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<form action = "{{ route('circuit.update', $post->idCircuit) }}" method = "POST">
<div class="modal-body">
@csrf
@method('PATCH')
<div class="form-group">
<label for="recipient-name" class="textblod">{{ __('Titre') }}</label>
<input type="text" class="form-control" name="title" value="{{$post->title}}" />
</div>
<div class="form-group">
<label for="post-description" class="textblod">{{ __('Activation') }}</label>
<label for="r2" class="textnormal">
<input type="radio" name="active" id="r2"onClick="getResults()" value="1"
@if($post->active == 1)
checked
@else
@endif > {{ __('Active') }}
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="active" id="r1" onClick="getResults()" value="0"
@if($post->active == 0)
checked
@else
@endif> {{ __('Bloqué') }}
</label>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
<button type="submit" class="model_btn_save"> {{ __('Sauvegarder') }}</button>
</div>
</form>

</div>
</div>
</div>

<tr id="post_id_{{ $post->idCircuit }}">
<td>
<a class="table_link" href="">
{{ $post->title  }}
</a>
</td>
<td>
@if(count($post->circuit_point))
{{count($post->circuit_point)}}
@else
0
@endif
</td>
<td>
{{ date('d/m/Y', strtotime($post->created_at)) }}
</td>
<td class="text-center">
@if($post->active =='1')
<a href=""><i class="fa fa-check active_color" aria-hidden="true"></i></a>
@else
<a href="{{ route('circuit.activate', $post->idCircuit) }}" ><i class="fa fa-lock desactive_color" aria-hidden="true"></i></a>
@endif
</td>

<td class="td_btn">
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit{{ $post->idCircuit }}" class="btn-delete"><i class="fa fa-edit"></i></a>
<a href="javascript:void(0)" data-toggle="modal" data-target="#delete{{ $post->idCircuit }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
@endsection

