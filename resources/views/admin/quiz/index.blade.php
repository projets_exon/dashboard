@extends('layouts.admin')
@section('title', 'Manage Quiz')
@section('content')
<?php $page = "Quiz";?>
<div class="container"></div>
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage Quiz</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home', app()->getLocale()) }}">Dashbord /</a>
<a id="pagetitlesecondelink">Quiz</a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Quiz ( {{ $Quiz->count() }} )</span>
</div>
<div class="col-md-5 text-right">
@if(Auth::user()->role=='3')
@else
<a href="{{ route('quiz.create', app()->getLocale()) }}" class="btn-switch" title="Add new"><i class="fa fa-plus" aria-hidden="true"></i></a>
@endif
<a class="btn-switch-active" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a>
</div>
</div>
<div class="space"></div>
<div class="space"></div>

@if(session()->get('success'))
<div class="alert {{ session()->get('success_alert') }} alert-dismissible fade show" role="alert">
<strong>{{ session()->get('success') }}</strong>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
@endif

<div class="row details">
<div class="col-12">
<table id="tableapp" class="table table-striped table-bordered" style="width:100%">
<thead>
<tr class="text-center">
<th>Question</th>
<th>Proposition 1</th>
<th>Proposition 2</th>
<th>Proposition 3</th>
<th>Proposition 4</th>
<th>Reponse</th>
<th>Point</th>
<th>Active</th>
<th>Action</th>
</tr>
</thead>
<tbody id="posts-crud">

@foreach($Quiz as $post)
<div class="modal fade" id="delete{{ $post->idQuiz }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-body" style="text-align:center;">
<h5 class="textbloddelete">Delete ?</h5>
<h5 class="textnormaldelete ">Please ensure and then confirm!</h5>
</div>
<div class="modal-footer">
<form action = "{{ route('quiz.destroy', $post->idQuiz) }}" method = "POST">
@csrf
@method('PATCH')
<a type="button" class="model_btn_close" data-dismiss="modal">No, cancel</a>
<button type="submit" class="model_btn_delete delete-post">Yes, Delete</button>
</form>
</div>
</div>
</div>
</div>



<div class="modal fade" id="edit{{ $post->idQuiz }}" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title textblod">Edit</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<form action = "{{ route('quiz.update', $post->idQuiz) }}" method = "POST">
<div class="modal-body">
@csrf
@method('PATCH')
<div class="form-group">
<label for="recipient-name" class="textblod">Title</label>
<input type="text" class="form-control" name="question" value="{{$post->question}}" />
</div>

<div class="form-group">
<label for="recipient-name" class="textblod">Proposition 1</label>
<input type="text" class="form-control" name="proposition_1" value="{{$post->proposition_1}}" />
</div>

<div class="form-group">
<label for="recipient-name" class="textblod">Proposition 2</label>
<input type="text" class="form-control" name="proposition_2" value="{{$post->proposition_2}}" />
</div>

<div class="form-group">
<label for="recipient-name" class="textblod">Proposition 3</label>
<input type="text" class="form-control" name="proposition_3" value="{{$post->proposition_3}}" />
</div>

<div class="form-group">
<label for="recipient-name" class="textblod">Proposition 3</label>
<input type="text" class="form-control" name="proposition_4" value="{{$post->proposition_4}}" />
</div>

<div class="form-group">
<label for="post-description" class="textblod">Activation</label>
<label for="r2" class="textnormal">
<input type="radio" name="active" id="r2"onClick="getResults()" value="1"
@if($post->active == 1)
checked
@else
@endif > Active
</label>

<label for="r1" class="textnormal" style="margin-left:20px;">
<input type="radio" name="active" id="r1" onClick="getResults()" value="0"
@if($post->active == 0)
checked
@else
@endif> Blocked
</label>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="model_btn_save">Save</button>
</div>
</form>

</div>
</div>
</div>

<tr id="post_id_{{ $post->idQuiz }}">
<td>
<a class="table_link" href="">
{{ $post->question  }}
</a>
</td>
<td>
<a class="table_link" href="">
{{ $post->proposition_1  }}
</a>
</td>
<td>
<a class="table_link" href="">
{{ $post->proposition_2  }}
</a>
</td>
<td>
<a class="table_link" href="">
{{ $post->proposition_3  }}
</a>
</td>
<td>
<a class="table_link" href="">
{{ $post->proposition_4  }}
</a>
</td>
<td>
<a class="table_link" href="">
@if($post->reponse == 1)
Proposition 1
@elseif($post->reponse == 2)
Proposition 2
@elseif($post->reponse == 3)
Proposition 3
@elseif($post->reponse == 4)
Proposition 4
@endif
</a>
</td>

<td>
{{ $post->point_quiz($post->point) }}
</td>

<td class="text-center">
@if($post->active =='1')
<a href="{{ route('quiz.destroy', $post->idQuiz) }}"><i class="fa fa-check active_color" aria-hidden="true"></i></a>
@else
<a href="{{ url('admin/quiz/activate', $post->idQuiz) }}/{{$post->point}}" ><i class="fa fa-lock desactive_color" aria-hidden="true"></i></a>
@endif
</td>

<td class="td_btn">
<a href="javascript:void(0)" data-toggle="modal" data-target="#edit{{ $post->idQuiz }}" class="btn-delete"><i class="fa fa-edit"></i></a>
<a href="javascript:void(0)" data-toggle="modal" data-target="#delete{{ $post->idQuiz }}" class="btn-delete delete-post"><i class="fa fa-trash"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
@endsection

