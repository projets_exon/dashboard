@extends('layouts.admin')
@section('title', 'Manage Quiz')
@section('content')
<?php $page = "Quiz";?>
<div class="container">
<div class="row titleapp">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Manage Quiz</span>
</div>
<div class="col-md-5" style="text-align:right;">
<a id="pagetitleprimerlink" href="{{ route('home', app()->getLocale()) }}">Dashbord /</a>
<a id="pagetitleprimerlink" href="{{ route('quiz.index', app()->getLocale()) }}">Quiz /</a>
<a id="pagetitlesecondelink">Add</a>
</div>
</div>

<div class="space"></div>
<div class="space"></div>

<div class="row cadre_add" style="padding:10px 0px 5px 0px;">
<div class="col-md-7">
<span class="mt-4 pagetitle" >Quiz</span>
</div>
<div class="col-md-5 text-right">
@if(Auth::user()->role=='3')
@else
<a class="btn-switch-active"><i class="fa fa-plus" aria-hidden="true"></i></a>
@endif
<a href="{{ route('quiz.index', app()->getLocale()) }}" class="btn-switch" title="Table"><i class="fa fa-table" aria-hidden="true"></i></a>
</div>
</div>

<div class="row titleappseconde">
<h3 class="page-title" style="padding:0px;">Add new Quiz</h3>
</div>



<div class="row">
<div class="col-lg-12 cadre_filter">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif

<form id="file-upload-form" class="uploader" action="{{ route('quiz.store', app()->getLocale()) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
@csrf
<div class="row">
<div class="form-group col-md-6">
<label for="name" class="col-sm-12 control-label textblod">Point</label>
<div class="col-sm-12">
<select type="text" id="point" name="point" class="form-control input-lg" required="">
<option value=""></option>
@foreach($point as $post)
<option value="{{ $post->idPoint }}">{{ $post->title }}</option>
@endforeach

</select>
</div>
</div>

<div class="form-group col-md-6">
<label for="name" class="col-sm-12 control-label textblod">Title</label>
<div class="col-sm-12">
<input type="text" id="question" name="question" class="form-control input-lg" required="">
<span id="error_email"></span>
</div>
</div>




<div class="form-group col-md-6">
<label for="home" class="col-sm-12 control-label textblod">Active</label>
<div class="col-sm-12">
<label for="1" class="textnormal">YES
<input type="radio" id="1" name="active"  value="1" checked>
</label>
<label for="2" class="textnormal margin20">NO
<input type="radio" id="2" name="active"  value="0" >
</label>
</div>
</div>
</div>




<div class="row">

<div class="row col-md-12">
<h3 class="page-title">Answers</h3>
</div>

<div class="col-md-12" style="padding:20px;">
<table class="table table-condensed">
<thead>
<tr>
<th>Title</th>
<th>Correct answer</th>
</tr>
</thead>

<tbody>
<tr>
<td>
<input type="text" id="proposition_1" name="proposition_1" class="form-control input-lg" required="">
</td>
<td>
<input type="radio" name="reponse"  value="1" >
</td>
</tr>

<tr>
<td>
<input type="text" id="proposition_1" name="proposition_2" class="form-control input-lg" required="">
</td>
<td>
<input type="radio" name="reponse"  value="2" >
</td>
</tr>

<tr>
<td>
<input type="text" id="proposition_1" name="proposition_3" class="form-control input-lg" required="">
</td>
<td>
<input type="radio" name="reponse"  value="3" >
</td>
</tr>

<tr>
<td>
<input type="text" id="proposition_1" name="proposition_4" class="form-control input-lg" required="">
</td>
<td>
<input type="radio" name="reponse"  value="4" >
</td>
</tr>


</tbody>
</table>
</div>



</div>

<div class="modal-footer"style="text-align:right;">
<button type="submit" name="register" id="register"  class="model_btn_save">Save</button>
</div>
</form>
</div>
</div>
</div>
<script>
$('div.form-group-max').maxlength();
</script>
@endsection
