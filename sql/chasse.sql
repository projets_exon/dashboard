-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 14 déc. 2021 à 02:02
-- Version du serveur : 10.4.20-MariaDB
-- Version de PHP : 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `chasse`
--

-- --------------------------------------------------------

--
-- Structure de la table `circuit`
--

CREATE TABLE `circuit` (
  `idCircuit` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `circuit`
--

INSERT INTO `circuit` (`idCircuit`, `title`, `active`, `created_at`, `updated_at`) VALUES
(1, 'gggg', 0, '2021-12-13 22:38:38', '2021-12-14 00:44:49'),
(2, 'gggg', 1, '2021-12-13 22:39:02', '2021-12-14 01:02:47'),
(3, 'circuit 99', 0, '2021-12-13 23:52:32', '2021-12-14 01:02:36');

-- --------------------------------------------------------

--
-- Structure de la table `language`
--

CREATE TABLE `language` (
  `idLanguage` int(11) NOT NULL,
  `labelLang` varchar(45) DEFAULT NULL,
  `symbole` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `active` int(255) NOT NULL,
  `bydefault` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `language`
--

INSERT INTO `language` (`idLanguage`, `labelLang`, `symbole`, `photo`, `active`, `bydefault`) VALUES
(1, 'fr', '', 'lgfr10121.png', 1, '1');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `playes`
--

CREATE TABLE `playes` (
  `idPlayes` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `circuit` int(11) NOT NULL,
  `current` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `point`
--

CREATE TABLE `point` (
  `idPoint` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `longitude` decimal(20,0) DEFAULT NULL,
  `lattitude` decimal(20,0) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `circuit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `point`
--

INSERT INTO `point` (`idPoint`, `title`, `description`, `longitude`, `lattitude`, `active`, `type`, `video`, `image`, `circuit`) VALUES
(1, 'point test', 'lorem  hfbnv', '12141', '12141', 1, NULL, NULL, NULL, 1),
(2, 'point test', 'lorem  hfbnv', '12141', '12141', 1, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `quiz`
--

CREATE TABLE `quiz` (
  `idQuiz` int(11) NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `proposition_1` varchar(255) DEFAULT NULL,
  `proposition_2` varchar(255) DEFAULT NULL,
  `proposition_3` varchar(255) DEFAULT NULL,
  `proposition_4` varchar(255) DEFAULT NULL,
  `reponse` varchar(255) DEFAULT NULL,
  `point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(45) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `status` int(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `languages` int(11) NOT NULL,
  `dashboard` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `pseudo`, `email`, `password`, `firstName`, `lastName`, `image`, `active`, `status`, `role`, `code`, `languages`, `dashboard`, `created_at`, `updated_at`) VALUES
(2, 'admin', 'admin@gmail.com', '$2y$10$X4JiiW.HWjcyIlVoNkl9Pu6ahk/N8BuNQeuKU4ar0LXQkJEEN501K', 'admin', 'admin', 'img99.jpg', 1, 1, 1, '0000', 1, '1', '2021-12-12 23:00:00', '2021-12-13 23:11:41');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `circuit`
--
ALTER TABLE `circuit`
  ADD PRIMARY KEY (`idCircuit`);

--
-- Index pour la table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`idLanguage`),
  ADD UNIQUE KEY `labelLang_UNIQUE` (`labelLang`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `playes`
--
ALTER TABLE `playes`
  ADD PRIMARY KEY (`idPlayes`),
  ADD KEY `fk_Playes_Users1_idx` (`user`),
  ADD KEY `fk_Playes_Circuit1_idx` (`circuit`),
  ADD KEY `fk_Playes_Point1_idx` (`current`);

--
-- Index pour la table `point`
--
ALTER TABLE `point`
  ADD PRIMARY KEY (`idPoint`),
  ADD KEY `fk_Point_Circuit1_idx` (`circuit`);

--
-- Index pour la table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`idQuiz`,`point`),
  ADD KEY `fk_Quiz_Point1_idx` (`point`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Users_Language_idx` (`languages`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `circuit`
--
ALTER TABLE `circuit`
  MODIFY `idCircuit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `language`
--
ALTER TABLE `language`
  MODIFY `idLanguage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `playes`
--
ALTER TABLE `playes`
  MODIFY `idPlayes` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `point`
--
ALTER TABLE `point`
  MODIFY `idPoint` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `idQuiz` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `playes`
--
ALTER TABLE `playes`
  ADD CONSTRAINT `fk_Playes_Circuit1` FOREIGN KEY (`circuit`) REFERENCES `circuit` (`idCircuit`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Playes_Point1` FOREIGN KEY (`current`) REFERENCES `point` (`idPoint`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Playes_Users1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `point`
--
ALTER TABLE `point`
  ADD CONSTRAINT `fk_Point_Circuit1` FOREIGN KEY (`circuit`) REFERENCES `circuit` (`idCircuit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `quiz`
--
ALTER TABLE `quiz`
  ADD CONSTRAINT `fk_Quiz_Point1` FOREIGN KEY (`point`) REFERENCES `point` (`idPoint`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_Users_Language` FOREIGN KEY (`languages`) REFERENCES `language` (`idLanguage`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
