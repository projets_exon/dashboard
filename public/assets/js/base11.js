var offset = 300, // browser window scroll (in pixels) after which the "back to top" link is shown
  offsetOpacity = 1200, //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
  scrollDuration = 700;

// When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar_top").style.padding = "10px";
    document.getElementById("logo").style.width = "240px";
    document.getElementById("logo").style.height = "90px";
	document.getElementById("navbar_top").style.boxShadow  = "10px 10px 5px 0px rgba(170, 169, 169, 0.75)";
	document.getElementById("search_r").style.position  = "";

	} else {
    document.getElementById("navbar_top").style.padding = "10px";
    document.getElementById("logo").style.width = "240px";
    document.getElementById("logo").style.height = "90px";
	document.getElementById("navbar_top").style.boxShadow  = "10px 10px 5px 0px transparent";
	document.getElementById("search_r").style.position  = "";
  }
}
